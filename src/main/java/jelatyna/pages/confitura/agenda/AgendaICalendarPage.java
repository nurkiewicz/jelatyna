package jelatyna.pages.confitura.agenda;

import jelatyna.domain.Agenda;
import jelatyna.domain.AgendaItem;
import jelatyna.domain.Presentation;
import jelatyna.repositories.AgendaRepository;
import jelatyna.repositories.PresentationRepository;
import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.model.property.Version;
import org.apache.wicket.markup.Markup;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.jsoup.Jsoup;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import static java.time.Month.*;
import static java.util.stream.Collectors.*;

@SuppressWarnings("serial")
public class AgendaICalendarPage extends WebPage {
    private static final LocalDate DATE_OF_CONFERENCE = LocalDate.of(2014, JUNE, 5);/*TODO: Zrobić to bardziej dynamiczne?*/
    private final Agenda agenda;
    @SpringBean
    private AgendaRepository agendaRepository;
    @SpringBean
    private PresentationRepository presentationRepository;

    public AgendaICalendarPage() {
        agenda = agendaRepository.getAgenda();

    }

    @SuppressWarnings("unchecked")
    private Calendar createCalendar() {
        Calendar calendar = new Calendar();
        calendar.getProperties().add(new ProdId("-//Confitura//iCal4j 1.0//EN"));
        calendar.getProperties().add(Version.VERSION_2_0);
        calendar.getProperties().add(CalScale.GREGORIAN);
        calendar.getComponents().addAll(createEvents());
        return calendar;
    }

    private VEvent createEventForPresentation(List<AgendaItem> items, int i, Date start, Date end) {
        AgendaItem item = items.get(i);
        Presentation presentation = presentationRepository.findOne(item.presentationId);
        String room = agenda.getRooms().get(i + 1);// 0 is 'godzina/sala'

        VEvent event = new VEvent(new DateTime(start), new DateTime(end), presentation.getTitle());
        event.getProperties().add(description(presentation));
        event.getProperties().add(new Uid(item.presentationId.toString()));
        event.getProperties().add(new Location(room));
        return event;
    }

    private Description description(Presentation presentation) {
        String description = presentation.getDescription();
        return new Description(Jsoup.parse(description).text());
    }

    private VEvent createEventForSlot(List<AgendaItem> items, int i, Date start, Date end) {
        VEvent event = new VEvent(new DateTime(start), new DateTime(end), items.get(i).getValue());
        event.getProperties().add(new Uid());
        return event;
    }

    private List<VEvent> createEvents() {
        List<VEvent> allEvents = new ArrayList<>();
        agenda.getSlots().stream().forEach(slot -> {
            String[] split = slot.getTime().split("-");
            Date start = getTime(split[0]);
            Date end = getTime(split[1]);
            List<AgendaItem> items = slot.getItems();
            List<VEvent> slotEvents = IntStream.range(0, items.size())
                    .boxed()
                    .map((i) -> createEvent(items, i, start, end))
                    .collect(toList());
            allEvents.addAll(slotEvents);
        });
        return allEvents;
    }

    private VEvent createEvent(List<AgendaItem> items, int i, Date start, Date end) {

        if (items.get(i).hasPresentation()) {
            return createEventForPresentation(items, i, start, end);
        } else {
            return createEventForSlot(items, i, start, end);
        }
    }

    @Override
    final protected void onRender() {
        HttpServletResponse response = (HttpServletResponse) getResponse().getContainerResponse();
        response.setContentType("text/calendar; charset=utf-8");
        try {
            Calendar calendar = createCalendar();
            CalendarOutputter output = new CalendarOutputter();
            if (response.getOutputStream() != null)
                output.output(calendar, response.getOutputStream());
            else
                response.setStatus(503);
        } catch (Exception ex) {
            Logger.getLogger(AgendaICalendarPage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public Markup getAssociatedMarkup() {
        return null;
    }

    @Override
    public void renderPage() {
        onRender();
    }

    private Date getTime(String timeString) {
        String pattern = timeString.contains(":") ? "HH:mm" : "HH";
        LocalTime time = LocalTime.parse(timeString, DateTimeFormatter.ofPattern(pattern));
        return toDate(DATE_OF_CONFERENCE, time);
    }

    private Date toDate(LocalDate date, LocalTime time) {
        return Date.from(LocalDateTime.of(date, time).atZone(ZoneId.systemDefault()).toInstant());
    }
}
