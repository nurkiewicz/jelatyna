package jelatyna.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@SuppressWarnings("serial")
@Entity
public class Sponsor extends AbstractEntity<Sponsor> implements WithFile {
    private String name;
    private String webPage;
    @Lob
    private String description;
    @Lob
    private String folderDescription;

    @ManyToOne(fetch = FetchType.EAGER)
    private SponsorType sponsorType;

    private Long money;

    public Sponsor() {
    }

    public Sponsor(String name, String webPage) {
        this.name = name;
        this.webPage = webPage;
    }

    public String getName() {
        return name;
    }

    public SponsorType getSponsorType() {
        return sponsorType;
    }

    public String getWebPage() {
        return webPage;
    }

    public String getDescription() {
        return description;
    }

    public Long getMoney() {
        return money;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Sponsor == false) {
            return false;
        }
        Sponsor other = (Sponsor) obj;
        return new EqualsBuilder().append(getId(), other.getId()).append(name, other.name).append(webPage, other.webPage).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(getId()).append(name).append(webPage).toHashCode();
    }

    @Override
    public String getSubfolder() {
        return "sponsors";
    }

    @Override
    public String getFileName() {
        return getId() + ".jpg";
    }

    public Sponsor sponsorType(SponsorType sponsorType) {
        this.sponsorType = sponsorType;

        return this;
    }

    public Sponsor order(int order) {

        return this;
    }

    public boolean isOf(SponsorType type) {
        return type.getName().equalsIgnoreCase(sponsorType.getName());
    }

    public Sponsor money(Long money) {
        this.money = money;
        return this;
    }

    public Sponsor description(String description) {
        this.description = description;
        return this;
    }
}
