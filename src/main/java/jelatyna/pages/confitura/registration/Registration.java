package jelatyna.pages.confitura.registration;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jelatyna.domain.Participant;
import jelatyna.domain.Presentation;
import jelatyna.domain.RegistrationConfiguration;
import jelatyna.domain.RegistrationStatus;
import jelatyna.pages.admin.registration.ParticipantMailSender;
import jelatyna.repositories.ParticipantRepository;
import jelatyna.repositories.PresentationRepository;
import jelatyna.repositories.RegistrationConfigurationRepository;
import jelatyna.services.RandomGenerator;

@Component
public class Registration {
    static final String PRESENTATION_LIMIT_ERROR = "Możesz wybrać maksymalnie 7 prezentacji!";
    static final String NOT_FOUND_ERROR = "Nie odnaleziono uczestnika dla podanego klucza!";
    static final String MAIL_EXISTS_ERROR = "Podany adres mailowy jest już zarejestrowany!";
    @Autowired
    private ParticipantMailSender mailSender;
    @Autowired
    private RandomGenerator randomGenerator;
    @Autowired
    private ParticipantRepository participantRepository;
    @Autowired
    private PresentationRepository presentationRepository;
    @Autowired
    private RegistrationConfigurationRepository configurationRepository;

    public boolean isActive() {
        return getConfiguration().isActive();
    }

    public List<Presentation> allPresentations() {
        return presentationRepository.findAllAccepted();
    }

    public String getCloseInfo() {
        return getConfiguration().getCloseInfo();
    }

    public void register(Participant participant) {
        if (participant.countPresentations() > 7) {
            throw new RuntimeException(PRESENTATION_LIMIT_ERROR);
        } else if (mailIsNotUnique(participant)) {
            throw new RuntimeException(MAIL_EXISTS_ERROR);
        }else if (isActive() == false){
            throw new RuntimeException("Rejestracja nie jest aktywna");
        }
        else {
            doRegister(participant);
        }
    }

    private void doRegister(Participant participant) {
        participant.token(randomGenerator.getRandomId());
        participantRepository.save(participant);
        mailSender.sendMessage(participant);
    }

    private boolean mailIsNotUnique(Participant participant) {
        return participantRepository.findByMail(participant.getMail()) != null;
    }

    private RegistrationConfiguration getConfiguration() {
        return configurationRepository.findAll().get(0);
    }

    public List<String> fetchCitiesStartingWith(String name) {
        return participantRepository.fetchCitiesStartingWith(name + "%");
    }

    public void changeStatusFor(String token, RegistrationStatus status) {
        Participant participant = participantRepository.findByToken(token);
        if (participant != null) {
            doChange(participant, status);
        } else {
            throw new RuntimeException(NOT_FOUND_ERROR);
        }
    }

    private void doChange(Participant participant, RegistrationStatus status) {
        participant.status(status);
        participantRepository.save(participant);
    }

    public void mailSender(ParticipantMailSender mailSender) {
        this.mailSender = mailSender;
    }

}
