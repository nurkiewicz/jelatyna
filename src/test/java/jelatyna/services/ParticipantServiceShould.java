package jelatyna.services;

import jelatyna.domain.Participant;
import jelatyna.domain.RegistrationStatus;
import jelatyna.pages.admin.registration.GroupMailSender;
import jelatyna.pages.admin.registration.Participants;
import jelatyna.repositories.ParticipantRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Lists.*;
import static jelatyna.TestUtils.*;
import static jelatyna.domain.RegistrationStatus.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ParticipantServiceShould {
    private List<Participant> participants = newArrayList(participant(1), participant(2), participant(3));
    @Mock
    private ParticipantRepository repository;
    @Mock
    private GroupMailSender sender;
    @InjectMocks
    private ParticipantService service = new ParticipantService();

    @Test
    public void findParticipantsByStatuses() {
        when(repository.findByRegistrationTypeIn(anyListOf(RegistrationStatus.class)))
                .thenReturn(participants);

        Participants foundParticipants = service.findByStatus(newArrayList(NEW));

        assertThat(foundParticipants.asList()).isSameAs(participants);
    }

    @Test
    public void fetchAllParticipantsIfStatusesListEmpty() {
        when(repository.findAll()).thenReturn(participants);

        Participants foundParticipants = service.findByStatus(new ArrayList<RegistrationStatus>());

        assertThat(foundParticipants.asList()).isSameAs(participants);
    }

    @Test
    public void sendMailToParticipantsWithGivenStatus() {
        Participant participant = participant(1);
        when(repository.findByRegistrationTypeIn(anyStatuses()))
                .thenReturn(newArrayList(participant));

        service.sendGroupMessage(newArrayList(NEW), false, false);

        verify(sender).sendMessages(newArrayList(participant));
    }

    @Test
    public void sendMailToParticipantsWithGivenStatusAndWithMailing() {
        Participant participant1 = participant(1).mailing(false);
        Participant participant2 = participant(2).mailing(true);
        when(repository.findByRegistrationTypeIn(anyStatuses()))
                .thenReturn(newArrayList(participant1, participant2));

        service.sendGroupMessage(newArrayList(NEW), true, false);

        verify(sender).sendMessages(newArrayList(participant2));
    }

    @Test
    public void sendMailToParticipantsWithGivenStatusAnParticipated() {
        Participant participant1 = participant(1).participated(true);
        Participant participant2 = participant(2).participated(false);
        when(repository.findByRegistrationTypeIn(anyStatuses()))
                .thenReturn(newArrayList(participant1, participant2));

        service.sendGroupMessage(newArrayList(NEW), false, true);

        verify(sender).sendMessages(newArrayList(participant1));
    }
}