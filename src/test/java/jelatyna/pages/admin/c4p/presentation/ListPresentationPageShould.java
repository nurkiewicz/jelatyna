package jelatyna.pages.admin.c4p.presentation;

import jelatyna.ConfituraSession;
import jelatyna.JelatynaTagTester;
import jelatyna.WicketTest;
import jelatyna.domain.Admin;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.services.AdminService;
import jelatyna.services.SpeakerService;
import org.apache.wicket.util.tester.TagTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.Locale;

import static com.google.common.collect.Lists.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ListPresentationPageShould extends WicketTest {
    @Mock
    private SpeakerService service;
    @Mock
    private AdminService adminService;

    @Override
    public void setupServices() {
        Locale.setDefault(new Locale("pl"));
        ConfituraSession.get().setUser(new Admin());
        put(service, adminService);
    }

    @Before
    public void setUpAppCtxAndStartPage() {
    }

    @Test
    public void displayPresentations() {
        List<Presentation> presentations = newArrayList(presentation(1), presentation(2));
        when(service.allPresentations()).thenReturn(presentations);

        tester.startPage(ListPresentationPage.class);

        List<TagTester> rows = tester.getTagsByWicketId("rows");
        assertThat(rows).hasSize(2);
        assertRowContainsPresentation(0, rows, presentations);
        assertRowContainsPresentation(1, rows, presentations);
    }

    @Test
    public void displayNumberOfVotesAndAverageRatePerPresentation() {
        List<Presentation> presentations = newArrayList(presentation(1));
        when(service.allPresentations()).thenReturn(presentations);
        when(service.countVotesFor(any(Presentation.class))).thenReturn(10);
        when(service.averageRateFor(any(Presentation.class))).thenReturn(1.55);

        tester.startPage(ListPresentationPage.class);

        List<TagTester> rows = tester.getTagsByWicketId("rows");
        JelatynaTagTester row = new JelatynaTagTester(rows.get(0));
        assertThat(row.getChildByWicketId("votes_count").getValue()).isEqualTo("10");
        assertThat(row.getChildByWicketId("votes_rate").getValue()).isEqualTo("1.55");
    }

    private void assertRowContainsPresentation(int idx, List<TagTester> rows, List<Presentation> presentations) {
        JelatynaTagTester row = new JelatynaTagTester(rows.get(idx));
        Presentation presentation = presentations.get(idx);
        assertThat(row.getChildByWicketId("idx").getValue()).isEqualTo((idx + 1) + "");
        assertThat(row.getChildByWicketId("title").getValue()).isEqualTo(presentation.getTitle());
        assertThat(row.getChildByWicketId("speaker").getValue()).isEqualTo(presentation.getOwner().getFullName());
    }

    private Presentation presentation(int id) {
        return new Presentation().id(id).title("Tytul " + id)
                .owner(new Speaker().firstName("Imie " + id).lastName("Nazwisko " + id));
    }

}
