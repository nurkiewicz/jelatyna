package jelatyna.pages.confitura.agenda;

import jelatyna.WicketTest;
import jelatyna.components.presentation.PresentationLinksPanel;
import jelatyna.domain.AgendaItem;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.services.FileService;
import jelatyna.services.PresentationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AgendaCellComponentShould extends WicketTest {
    private Presentation presentation = new Presentation().id(1).title("title")
            .owner(new Speaker().firstName("Jan").lastName("Kowalski"));
    @Mock
    private PresentationService service;
    @Mock
    private FileService fileService;

    @Override
    public void setupServices() {
        put(service, fileService);
        when(service.findBy(anyInt())).thenReturn(presentation);
    }

    @Test
    public void renderValueIfPresentationNotSet() {
        AgendaItem cell = new AgendaItem("coffe break");

        tester.startComponentInPage(new AgendaCellComponent("id", cell));

        tester.assertLabel("id:value", cell.getValue());
        tester.assertInvisible("id:presentation");
    }

    @Test
    public void renderLinkToPresentationAndSpeakerNameIfCellContainsPresentation() {
        AgendaItem cell = new AgendaItem(1);

        tester.startComponentInPage(new AgendaCellComponent("id", cell));

        tester.assertInvisible("id:value");
        assertThat(tester.getTagByWicketId("link").getAttribute("href")).isEqualTo("./presentations#1");
        tester.assertLabel("id:presentation:link:title", presentation.getTitle());
        tester.assertLabel("id:presentation:speaker", presentation.getOwner().getFullName());
        tester.assertComponent("id:presentation:links", PresentationLinksPanel.class);
    }

}
