package jelatyna.pages.admin.lottery;

import jelatyna.components.nogeneric.Link;
import jelatyna.domain.Participant;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.LotteryService;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static java.lang.String.*;
import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class LotteryPage extends AdminBasePage {

    @SpringBean
    private LotteryService service;
    private DrawModel model = new DrawModel();

    public LotteryPage() {
        Form<Participant> form = new Form<Participant>("form", new CompoundPropertyModel<Participant>(model));
        form.add(label("firstName").setOutputMarkupId(true));
        form.add(label("lastName").setOutputMarkupId(true));
        form.add(label("mail"));
        add(form);
        add(new DrawButton("draw", form));
        add(new ResetLink("reset"));
    }

    private void shuffleLabel(AjaxRequestTarget target, String labelName, int delay) {
        target.appendJavaScript(format("$('#%s').hide();", labelName));
        target.appendJavaScript(format("setTimeout(\"shuffle('#%s')\",%d)", labelName, delay));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.LOTTERY;
    }

    @Override
    public String getPageTitle() {
        return "Losowanie";
    }

    private final class ResetLink extends Link {
        private ResetLink(String id) {
            super(id);
        }

        @Override
        public void onClick() {
            service.clear();
        }
    }

    private final class DrawButton extends AjaxLink<Void> {
        private final Form<Participant> form;

        private DrawButton(String id, Form<Participant> form) {
            super(id);
            this.form = form;
        }

        @Override
        public void onClick(AjaxRequestTarget target) {
            if (target != null) {
                model.doLoad();
                shuffleLabel(target, "firstName", 100);
                shuffleLabel(target, "lastName", 2000);
                target.add(form);
            }
        }
    }

    private final class DrawModel extends LoadableDetachableModel<Participant> {

        private Participant drown = Participant.EMPTY;

        @Override
        protected Participant load() {
            return drown;
        }

        public void doLoad() {
            drown = service.draw();
        }
    }
}
