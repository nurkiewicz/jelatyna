package jelatyna;

import jelatyna.domain.Admin;
import jelatyna.domain.Speaker;
import org.apache.wicket.request.Request;
import org.junit.Before;
import org.junit.Test;

import java.util.Locale;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;


public class ConfituraSessionShould {
    private Request request = mock(Request.class);
    private ConfituraSession session;

    @Before
    public void setupLocale() {
        when(request.getLocale()).thenReturn(new Locale("pl"));
        session = new ConfituraSession(request);
    }

    @Test
    public void findLoggedInAdmin() {
        session.setUser(new Admin());

        assertThat(session.hasAdmin()).isTrue();
        assertThat(session.hasSpeaker()).isFalse();
    }

    @Test
    public void findLoggedInUser() {
        session.setUser(new Speaker());

        assertThat(session.hasSpeaker()).isTrue();
        assertThat(session.hasAdmin()).isFalse();
    }
}
