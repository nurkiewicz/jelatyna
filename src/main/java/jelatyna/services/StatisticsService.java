package jelatyna.services;

import jelatyna.repositories.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatisticsService {
    @Autowired
    private ParticipantRepository repository;

    public ParticipantsProvider getParticipantsProvider() {
        return new ParticipantsProvider(repository.findAll());
    }
}
