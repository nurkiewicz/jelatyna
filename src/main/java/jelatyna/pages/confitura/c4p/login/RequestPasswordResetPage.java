package jelatyna.pages.confitura.c4p.login;

import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.services.SpeakerService;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

@SuppressWarnings("serial")
public class RequestPasswordResetPage extends BaseWebPage {

    @SpringBean
    private SpeakerService service;

    public RequestPasswordResetPage() {
        add(i18nLabel("password.forgot.label"));
        add(new RequestPasswordResetForm("form"));
        setPageTitlePostfix(new ResourceModel("password.forgot.label"));
    }

    private final class RequestPasswordResetForm extends Form<Void> {

        private TextField<String> mail = withLabelKey("email.label", textField("mail", model()));

        private RequestPasswordResetForm(String id) {
            super(id);
            add(new ConfituraFeedbackPanel("feedback"));
            add(i18nLabel("email.label"), mail);
        }

        @Override
        protected void onSubmit() {
            try {
                service.requestPasswordResetBy(mail.getValue());
                getSession().info(new ResourceModel("password.reset.request.message").getObject());
                setResponsePage(LoginSpeakerPage.class);
            } catch (Exception ex) {
                error(ex.getMessage());
            }
        }
    }
}
