package jelatyna.pages.admin.news;

import jelatyna.components.DeleteEntityLink;
import jelatyna.components.RedirectLink;
import jelatyna.domain.News;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.NewsService;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class ListNewsPage extends AdminBasePage {

    @SpringBean
    private NewsService service;

    public ListNewsPage() {
        add(new NewsGrid());
    }

    private String getAutorName(News news) {
        return news.getAutor() != null ? news.getAutor().getFullName() : "";
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CONFERENCE_SITE;
    }

    @Override
    public String getPageTitle() {
        return "Aktualności";
    }

    private final class NewsGrid extends DataView<News> {
        private NewsGrid() {
            super("rows", new ListDataProvider<News>(service.findAll()));
        }

        @Override
        protected void populateItem(Item<News> item) {
            News news = item.getModelObject();
            item.add(label("title", news.getTitle()));
            item.add(label("author", getAutorName(news)));
            item.add(label("date", news.formatCreatioDateWithTime()));
            item.add(yesNoIcon("published", news.isPublished()));
            item.add(new RedirectLink("edit", AddNewsPage.class, news));
            item.add(new DeleteEntityLink(news, ListNewsPage.class) {
                @Override
                protected void deleteById(Integer id) {
                    service.deleteById(id);
                }
            });

        }

    }
}
