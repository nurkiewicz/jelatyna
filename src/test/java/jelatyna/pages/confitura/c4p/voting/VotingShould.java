package jelatyna.pages.confitura.c4p.voting;

import jelatyna.ConfituraSession;
import jelatyna.domain.Presentation;
import jelatyna.domain.Vote;
import jelatyna.services.RandomGenerator;
import jelatyna.services.SpeakerService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static com.google.common.collect.Lists.*;
import static jelatyna.pages.confitura.c4p.voting.Voting.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class VotingShould {
    private List<Presentation> presentations = newArrayList(new Presentation().id(1), new Presentation().id(2));
    @Mock
    private ConfituraSession session;
    @Mock
    private HttpProvider httpProvider;
    @Mock
    private RandomGenerator randomizeProvider;
    @Mock
    private SpeakerService speakerService;
    @InjectMocks
    private Voting voting = new Voting();

    @Before
    public void setupSessionProvider() {
        when(httpProvider.getSession()).thenReturn(session);
    }

    @Test
    public void fetchShuffledPresentationsFromServiceAndPutInSessionIfNotAvailableYet() {
        when(speakerService.allShuffledPresentations()).thenReturn(presentations);
        mockSessionWith(null);

        List<Presentation> fetchedPresentations = voting.fetchPresentations();

        verify(session).setPresentation(presentations);
        assertThat(fetchedPresentations).isSameAs(presentations);
    }

    @Test
    public void fetchPresentationsFromSessionIfAvailable() {
        mockSessionWith(presentations);

        List<Presentation> fetchedPresentations = voting.fetchPresentations();

        verify(speakerService, never()).allShuffledPresentations();
        assertThat(fetchedPresentations).isSameAs(presentations);
    }

    @Test
    public void fetchFirstPresentation() {
        spyVotingWithPresentations();

        Presentation presentation = voting.getPresentation(0);

        assertThat(presentation).isSameAs(presentations.get(0));
    }

    @Test
    public void fetchSecondPresentation() {
        spyVotingWithPresentations();

        Presentation presentation = voting.getPresentation(1);

        assertThat(presentation).isSameAs(presentations.get(1));
    }

    @Test
    public void notBeLastpresentation() {
        spyVotingWithPresentations();

        boolean last = voting.isLast(0);

        assertThat(last).isFalse();
    }

    @Test
    public void beLastPresentation() {
        spyVotingWithPresentations();

        boolean last = voting.isLast(1);

        assertThat(last).isTrue();
    }

    @Test
    public void saveVoteUnderNewKey() {
        Presentation presentation = new Presentation();
        String id = "123";
        when(httpProvider.getCookieValue(ID_KEY)).thenReturn(null);
        when(randomizeProvider.getRandomId()).thenReturn(id);

        voting.save(presentation, 1);

        verify(httpProvider).setCookieValue("id", id);
        verify(speakerService).save(new Vote(id, presentation, 1));
    }

    @Test
    public void saveVoteUnderKeyFromCookie() {
        Presentation presentation = new Presentation();
        String id = "123";
        when(httpProvider.getCookieValue(ID_KEY)).thenReturn(id);

        voting.save(presentation, 1);

        verify(httpProvider, never()).setCookieValue(anyString(), anyString());
        verify(speakerService).save(new Vote(id, presentation, 1));
    }

    private void spyVotingWithPresentations() {
        voting = spy(voting);
        doReturn(presentations).when(voting).fetchPresentations();
    }

    private void mockSessionWith(List<Presentation> presentations) {
        when(session.getPresentations()).thenReturn(presentations);
    }
}
