package jelatyna.domain.dto;

import java.util.List;

public class AgendaDto extends AbstractDto{
    private List<String> rooms;
    private List<TimeSlotDto> slots;
    private List<AgendaScheduleDto> schedule;

    public List<String> getRooms() {
        return rooms;
    }

    public void setRooms(List<String> rooms) {
        this.rooms = rooms;
    }

    public List<TimeSlotDto> getSlots() {
        return slots;
    }

    public void setSlots(List<TimeSlotDto> slots) {
        this.slots = slots;
    }

    public List<AgendaScheduleDto> getSchedule() {
        return schedule;
    }

    public void setSchedule(List<AgendaScheduleDto> schedule) {
        this.schedule = schedule;
    }

    public AgendaDto rooms(final List<String> rooms) {
        this.rooms = rooms;
        return this;
    }

    public AgendaDto slots(final List<TimeSlotDto> slots) {
        this.slots = slots;
        return this;
    }

    public AgendaDto schedule(final List<AgendaScheduleDto> schedule) {
        this.schedule = schedule;
        return this;
    }


}
