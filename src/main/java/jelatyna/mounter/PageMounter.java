package jelatyna.mounter;

import jelatyna.Confitura;

public class PageMounter {

    public PageMounter(Confitura confitura) {
        new StandardModuleMounter(confitura).mount();
        new AdminModuleMounter(confitura).mount();
        new VolunteerModuleMounter(confitura).mount();
    }

}
