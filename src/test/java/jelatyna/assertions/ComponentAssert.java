package jelatyna.assertions;

import org.apache.wicket.Component;

import static org.assertj.core.api.Assertions.*;

public class ComponentAssert {

    private Component component;

    public ComponentAssert(Component component) {
        this.component = component;
    }

    public ComponentAssert isInstanceOf(Class<? extends Component> clazz) {
        assertThat(component).isInstanceOf(clazz);
        return this;
    }

    public void hasContent(String content) {
        assertThat(component.getDefaultModelObjectAsString()).isEqualTo(content);
    }

}
