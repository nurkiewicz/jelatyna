package jelatyna.pages.admin.c4p.presentation;

import jelatyna.components.RedirectLink;
import jelatyna.components.nogeneric.Link;
import jelatyna.domain.Presentation;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.SpeakerService;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class ListPresentationPage extends AdminBasePage {

    @SpringBean
    private SpeakerService service;

    public ListPresentationPage() {
        add(new PresentationList(service.allPresentations()));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CALL_4_PAPERS;
    }

    @Override
    public String getPageTitle() {
        return "Prezentacje";
    }

    private final class PresentationList extends ListView<Presentation> {

        private PresentationList(List<Presentation> list) {
            super("rows", list);
        }

        @Override
        protected void populateItem(ListItem<Presentation> item) {
            final Presentation presentation = item.getModelObject();
            item.add(label("idx", item.getIndex() + 1));
            item.add(label("title", presentation.getTitle()));
            item.add(label("speaker", presentation.getOwner().getFullName()));
            item.add(label("votes_count", service.countVotesFor(presentation)));
            item.add(label("votes_rate", service.averageRateFor(presentation)));
            item.add(yesNoIcon("accepted", presentation.isAccepted()));
            item.add(new RedirectLink("view", ViewPresentationPage.class, presentation));
            item.add(new RedirectLink("edit", EditPresentationPage.class, presentation));
            Link acceptedLink = new Link("accept") {
                @Override
                public void onClick() {
                    service.toggleAcceptance(presentation);
                    setResponsePage(ListPresentationPage.class);
                }
            };
            item.add(acceptedLink.add(label("label", getLabel(presentation)).setRenderBodyOnly(true)));
        }

        private String getLabel(Presentation presentation) {
            return presentation.isAccepted() ? "nie akceptuj" : "akceptuj";
        }
    }
}
