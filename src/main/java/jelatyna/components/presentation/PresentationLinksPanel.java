package jelatyna.components.presentation;

import jelatyna.domain.Presentation;
import jelatyna.services.FileService;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;

@SuppressWarnings("serial")
public class PresentationLinksPanel extends Panel {
    @SpringBean
    private FileService fileService;

    public PresentationLinksPanel(String id, Presentation presentation) {
        super(id);
        add(new ExternalLink("download", fileService.getUrlTo(presentation)).setVisible(presentation.hasFile()));
        add(new ExternalLink("youTube", presentation.getYouTube()).setVisible(presentation.hasYouTube()));
        add(new ExternalLink("parleys", presentation.getParleys()).setVisible(presentation.hasParleys()));
    }

}
