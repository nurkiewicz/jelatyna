package jelatyna.pages.confitura.c4p;

import jelatyna.Confitura;
import jelatyna.TestApplicationWithSpringContextMock;
import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.util.file.File;
import org.apache.wicket.util.lang.Bytes;
import org.apache.wicket.util.tester.FormTester;
import org.apache.wicket.util.tester.WicketTester;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import static org.assertj.core.api.Assertions.*;

public abstract class AbstractPhotoUploadSizeValidationTest {

    protected WicketTester tester = new WicketTester(TestApplicationWithSpringContextMock.withoutInjector());

    private static File getFileLargerThan(Bytes requiredFileSize) {
        File lena = toWicketFile(new ClassPathResource("/testData/images/lena.png"));
        assertThat(lena.length()).isGreaterThan(requiredFileSize.bytes());
        return lena;
    }

    private static File toWicketFile(Resource resource) {
        try {
            return new File(resource.getFile());
        } catch (IOException e) {
            throw new RuntimeException("Could not obtain resource's file", e);
        }
    }

    protected abstract Panel createPhotoUploadFormPanelUnderTest();

    public void shouldNotAllowPhotosBiggerThanUploadedPhotoMaxSize() {
        // given
        tester.startComponentInPage(createPhotoUploadFormPanelUnderTest());
        FormTester speakerFormTester = tester.newFormTester("userForm:form");
        File tooBigPhoto = getFileLargerThan(Confitura.UPLOADED_PHOTO_MAX_SIZE);

        // when
        speakerFormTester.setFile("photoUpload", tooBigPhoto, "image/png");
        speakerFormTester.submit();

        // then
        List<Serializable> errorMessages = tester.getMessages(FeedbackMessage.ERROR);
        assertThat(errorMessages).contains("Upload must be less than 300K");
    }
}
