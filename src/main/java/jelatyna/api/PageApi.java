package jelatyna.api;

import jelatyna.domain.SimpleContent;
import jelatyna.domain.dto.PageDto;
import jelatyna.repositories.SimpleContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PageApi {
    @Autowired
    private SimpleContentRepository repository;

    @RequestMapping(value = "pages/{name}", method = RequestMethod.GET)
    @ResponseBody
    public PageDto getPage(@PathVariable String name) {
        SimpleContent simpleContent = repository.findByTitle(name);
        return new PageDto(simpleContent.getContent());
    }
}
