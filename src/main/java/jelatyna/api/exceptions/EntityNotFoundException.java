package jelatyna.api.exceptions;

public class EntityNotFoundException extends RuntimeException{
    public EntityNotFoundException() {
        super("Not Found");
    }
}
