package jelatyna.pages.admin;

import jelatyna.utils.Components;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;

@SuppressWarnings("serial")
public class AdminPageTitlePanel extends Panel {
    public AdminPageTitlePanel(String id, String currentPageTitle) {
        super(id);

        Label titleHeader = Components.label("pageTitleHeader", currentPageTitle);
        titleHeader.setRenderBodyOnly(true);

        add(titleHeader);
        add(Components.label("pageTitle", currentPageTitle));
    }
}
