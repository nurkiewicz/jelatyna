package jelatyna.utils;

import jelatyna.domain.WithMail;
import jelatyna.repositories.MailTemplateRepository;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.StringWriter;
import java.util.Properties;

import static javax.mail.Message.RecipientType.*;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class MailSender {
    protected static final String MAIN_ADDRESS = "confitura@confitura.pl";
    protected static final String CHARSET = "UTF-8";
    protected VelocityContext ctx = new VelocityContext();
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private MailTemplateRepository repository;
    @Value("${conference.year}")
    private String conferenceYear;
    private Session session;

    private String templateName;

    public MailSender() {
        Properties properties = new Properties();
        properties.setProperty("mail.smtp.host", "localhost");
        session = Session.getDefaultInstance(properties);
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public void sendMessage() {
        send(MAIN_ADDRESS);
    }

    public void sendMessage(WithMail recipient) {
        send(recipient.getMail());
    }

    private void send(String address) {
        try {
            logger.info("Sending to " + address);
            Message msg = createMessage(address);
            Transport.send(msg, msg.getAllRecipients());
            logger.info("Message sent");
        } catch (Exception ex) {
            logger.error(String.format("Exception while sending message to [%s]", address), ex);
        }
    }

    private Message createMessage(String address) throws Exception {
        InternetAddress from = new InternetAddress(MAIN_ADDRESS, "Confitura " + conferenceYear, CHARSET);
        MimeMessage msg = new MimeMessage(session);
        msg.setReplyTo(new Address[]{from});
        msg.setFrom(from);
        msg.setRecipient(TO, new InternetAddress(address));
        msg.setContent(getContent(), "text/html; charset=UTF-8");
        msg.setSubject(getSubject(), CHARSET);
        return msg;
    }

    public String getContent() {
        return getText(getTemplate());
    }

    public String getText(String template) {
        try {
            StringWriter message = new StringWriter();
            Velocity.evaluate(ctx, message, "message", template);
            return message.toString();
        } catch (Exception ex) {
            throw new RuntimeException("Exception while evaluating message template [%s]", ex);
        }
    }

    public String getSubject() {
        return getText(getSubjectTemplate());
    }

    public String getSubjectTemplate() {
        return repository.readByType(templateName).getSubject();
    }

    public String getTemplate() {
        return repository.readByType(templateName).getTemplate();
    }

    public void set(String name, Object value) {
        ctx.put(name, value);
    }

}
