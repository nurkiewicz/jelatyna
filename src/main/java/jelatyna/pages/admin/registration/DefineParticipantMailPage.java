package jelatyna.pages.admin.registration;

import jelatyna.components.mail.DefineMailPanel;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;

@SuppressWarnings("serial")
public class DefineParticipantMailPage extends AdminBasePage {

    public DefineParticipantMailPage() {
        add(new DefineMailPanel("mail", "participant", "$firstName, $lastName, $link"));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.PARTICIPANTS;
    }

    @Override
    public String getPageTitle() {
        return "E-mail rejestracyjny";
    }
}
