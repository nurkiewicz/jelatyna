package jelatyna.pages.confitura.speaker;

import jelatyna.components.user.speaker.ViewPresentationPanel;
import jelatyna.domain.Presentation;
import jelatyna.pages.confitura.BaseWebPage;
import jelatyna.repositories.PresentationRepository;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.Collections;
import java.util.List;

@SuppressWarnings("serial")
public class ListPresentationPage extends BaseWebPage {
    @SpringBean
    private PresentationRepository repository;

    public ListPresentationPage() {
        List<Presentation> presentations = repository.findAllAccepted();
        Collections.shuffle(presentations);
        add(new PresentationsList("presentations", presentations));
        setPageTitlePostfix("Prezentacje");
    }

    private final class PresentationsList extends ListView<Presentation> {
        private PresentationsList(String id, List<Presentation> list) {
            super(id, list);
        }

        @Override
        protected void populateItem(ListItem<Presentation> item) {
            item.add(ViewPresentationPanel.createPanelWithLinkToSpeaker(item.getModelObject()));
        }

    }
}
