package jelatyna.api;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.emory.mathcs.backport.java.util.Collections;
import jelatyna.domain.dto.UserDto;
import jelatyna.domain.dto.converters.UserConverter;
import jelatyna.services.AdminService;
import jelatyna.services.VolunteerService;

@Controller
public class HostsApi {
    @Autowired
    private AdminService adminService;
    @Autowired
    private VolunteerService volunteerService;
    @Autowired
    private UserConverter userConverter;

    @RequestMapping(value = "hosts/main", method = RequestMethod.GET)
    @ResponseBody
    public List<UserDto> getHosts() {
        List<UserDto> users = adminService.findAll().stream()
                .map((admin) -> {
                    return userConverter.map(admin);
                })
                .collect(Collectors.toList());
        Collections.shuffle(users);
        return users;
    }

    @RequestMapping(value = "hosts/volunteers", method = RequestMethod.GET)
    @ResponseBody
    public List<UserDto> getVolunteers() {
        return volunteerService.findAll().stream()
                .map((admin) -> {
                    return userConverter.map(admin);
                })
                .collect(Collectors.toList());
    }

}
