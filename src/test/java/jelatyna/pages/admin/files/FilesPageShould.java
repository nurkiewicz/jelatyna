package jelatyna.pages.admin.files;

import jelatyna.JelatynaTagTester;
import jelatyna.WicketTest;
import jelatyna.services.AdminService;
import jelatyna.services.FileService;
import org.apache.wicket.util.tester.FormTester;
import org.apache.wicket.util.tester.TagTester;
import org.junit.Before;
import org.junit.Test;
import org.mockito.stubbing.OngoingStubbing;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Lists.*;
import static jelatyna.TestUtils.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class FilesPageShould extends WicketTest {

    private static final String ITEM_PATH = "files_panel:files:";
    private FileService fileService = mock(FileService.class);

    @Override
    @Before
    public void setupServices() {
        withAdmin();
        put(fileService, mock(AdminService.class));
    }

    @Test
    public void listAllFoldersInRootFolder() {
        List<File> folders = folders("folder_1", "folder_2");
        filesIn(anyListOf(String.class)).thenReturn(folders);

        tester.startPage(FilesPage.class);

        assertPathIs("ROOT");
        fileItem(0).isFolder().hasName("folder_1");
        fileItem(1).isFolder().hasName("folder_2");
    }

    @Test
    public void listAllFoldersInSelectedFolder() {
        List<File> folders1 = folders("folder_1", "folder_2");
        List<File> folders2 = folders("folder_3");
        filesIn(new ArrayList<String>()).thenReturn(folders1);
        filesIn(newArrayList("folder_1")).thenReturn(folders2);
        tester.startPage(FilesPage.class);

        tester.clickLink(pathToFolder(0));

        assertPathIs("ROOT /folder_1");
        fileItem(0).isFolder().hasName("folder_3");
    }

    @Test
    public void listFilesInRootFolder() {
        List<File> files = files(fileName(0), fileName(1));
        withPathTo(fileName(0), path(0));
        withPathTo(fileName(1), path(1));
        filesIn(anyListOf(String.class)).thenReturn(files);

        tester.startPage(FilesPage.class);

        fileItem(0).isFile().hasName(fileName(0)).hasPath(path(0));
        fileItem(1).isFile().hasName(fileName(1)).hasPath(path(1));
        verify(fileService).getUrlTo(new ArrayList<String>(), fileName(0));
        verify(fileService).getUrlTo(new ArrayList<String>(), fileName(1));
    }


    @Test
    public void addFolderToCurrentPath() {
        List<File> folders = folders("folder_1");
        filesIn(anyListOf(String.class)).thenReturn(folders);
        tester.startPage(FilesPage.class);
        tester.clickLink(pathToFolder(0));
        FormTester form = tester.newFormTester("form");

        form.setValue("folder", "new folder");
        form.submit();

        verify(fileService).newFolder(newArrayList("folder_1"), "new folder");
    }

    private FileIconChecker fileItem(int idx) {
        return new FileIconChecker(idx);
    }

    private List<File> folders(String... names) {
        List<File> folders = newArrayList();
        for (String name : names) {
            folders.add(folder(name));
        }
        return folders;
    }

    private List<File> files(String... names) {
        List<File> files = newArrayList();
        for (String name : names) {
            files.add(file(name));
        }
        return files;
    }

    private OngoingStubbing<List<File>> filesIn(List<String> folders) {
        return when(fileService.listFilesIn(folders));
    }

    private void assertPathIs(String path) {
        JelatynaTagTester tag = new JelatynaTagTester(tester.getTagByWicketId("folders"));
        tag.assertHasText(path);
    }

    private String path(int id) {
        return "path" + id;
    }

    private String fileName(int id) {
        return "file_" + id;
    }

    private void withPathTo(String file1, String path1) {
        when(fileService.getUrlTo(anyListOf(String.class), eq(file1))).thenReturn(path1);
    }

    private String fileToFile(int idx) {
        return ITEM_PATH + idx + ":file";
    }

    private String pathToFolder(int id) {
        return ITEM_PATH + id + ":folder";
    }

    public class FileIconChecker {

        private int idx;
        private List<TagTester> tagTester;

        public FileIconChecker(int idx) {
            this.idx = idx;
            tagTester = tester.getTagsByWicketId("file");
        }

        public FileIconChecker isFolder() {
            tester.assertInvisible(fileToFile(idx));
            tester.assertVisible(pathToFolder(idx));
            return this;
        }

        public FileIconChecker isFile() {
            tester.assertInvisible(pathToFolder(idx));
            tester.assertVisible(fileToFile(idx));
            return this;
        }

        public FileIconChecker hasName(String fileName) {
            tester.assertLabel(ITEM_PATH + idx + ":name", fileName);
            return this;
        }

        public FileIconChecker hasPath(String path) {
            assertThat(tagTester.get(idx).getAttribute("src")).isEqualTo(path);
            return this;
        }

    }
}