package jelatyna.services;

import jelatyna.domain.News;
import jelatyna.repositories.NewsRepository;
import jelatyna.repositories.PublishedNews;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsService {
    public static final Sort BY_CREATION_DATE = new Sort(Direction.DESC, "creationDate");
    public static final PublishedNews PUBLISHED = new PublishedNews();

    @Autowired
    private NewsRepository repository;

    @Cacheable(value = "news", key = "#root.methodName")
    public Long countPublished() {
        return repository.count(PUBLISHED);
    }

    @Cacheable(value = "news", key = "#root.methodName")
    public List<News> fetchPublished() {
        return repository.findAll(PUBLISHED);
    }

    @Cacheable(value = "news", key = "#root.methodName")
    public List<News> findAll() {
        return repository.findAll(BY_CREATION_DATE);
    }

    public Page<News> findPublished(int pageNo, int pageSize) {
        return repository.findAll(PUBLISHED, new PageRequest(pageNo, pageSize, BY_CREATION_DATE));
    }

    @Cacheable(value = "news", key = "{#root.methodName, #title}")
    public News findByTitle(String title) {
        return repository.findByTitle(title);
    }

    @Cacheable(value = "news", key = "{#root.methodName, #id}")
    public News findById(int id) {
        return repository.findOne(id);
    }

    @Cacheable(value = "news", key = "{#root.methodName, #id}")
    public News findPublishedById(int id) {
        return repository.findByIdAndPublished(id, true);
    }

    @NewsCacheEvict
    public void save(News news) {
        repository.save(news);
    }

    @NewsCacheEvict
    public void deleteById(Integer id) {
        repository.delete(id);
    }

}
