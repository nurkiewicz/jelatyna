package jelatyna.utils;

public class DateUtils {
    public static final String DATE_FORMAT = "dd.MM.yyyy";
    public static final String TIME_FORMAT = "HH:mm " + DATE_FORMAT;
    public static final String HOUR_FORMAT = "HH, " + DATE_FORMAT;
}
