package jelatyna.components.menu;

import jelatyna.domain.MainMenu;
import jelatyna.domain.MenuItem;
import jelatyna.repositories.MenuRepository;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;

import static jelatyna.utils.Components.*;

@SuppressWarnings({"serial"})
public class MainMenuPanel extends Panel {

    private MenuRepository menuRepository;
    private MenuLinks menuItemList;

    public MainMenuPanel(String id, MenuRepository menuRepository, MenuLinks menuItemList) {
        super(id);
        this.menuRepository = menuRepository;
        this.menuItemList = menuItemList;
        add(new ListView<MenuItem>("menuItems", getMainMenu().getChildren()) {
            @Override
            protected void populateItem(ListItem<MenuItem> item) {
                MenuItem menuItem = item.getModelObject();
                if (menuItem.hasChildren()) {
                    item.add(new AttributeModifier("class", "submenu"));
                }
                item.setVisible(menuItem.isPublished());
                item.add(createLink("menuItem", menuItem).setRenderBodyOnly(true));
                MarkupContainer submenu = new WebMarkupContainer("submenu");
                submenu.setVisible(menuItem.hasChildren());
                item.add(submenu);
                submenu.add(new ListView<MenuItem>("submenuItems", menuItem.getChildren()) {
                    @Override
                    protected void populateItem(ListItem<MenuItem> item) {
                        MenuItem submenu = item.getModelObject();
                        item.setVisible(submenu.isPublished());
                        item.add(createLink("submenuItem", submenu));
                    }
                });
            }
        });
    }

    private MenuItem getMainMenu() {
        MainMenu mainMenu = menuRepository.getMainMenu();
        return mainMenu != null ? mainMenu : new MainMenu();
    }

    private Component createLink(String id, MenuItem menuItem) {
        MenuLink menuLink = menuItemList.getMenuLinkFor(menuItem);
        if (menuLink != null) {
            return new MenuVisualLink(id, menuItem, menuLink);
        }
        return label(id, menuItem.getName());
    }

}
