package jelatyna.pages.admin.agenda;

import jelatyna.domain.Agenda;
import org.apache.wicket.Page;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.model.IModel;

@SuppressWarnings("serial")
abstract class ChooseOptionsModalWindow extends ModalWindow {
    public ChooseOptionsModalWindow(String id, final IModel<Agenda> agendaModel, final int colNum,
                                    final int rowNum) {
        super(id);
        setPageCreator(createPageCreator(agendaModel, colNum, rowNum));
        setInitialHeight(500);
        setInitialWidth(500);
    }

    protected PageCreator createPageCreator(final IModel<Agenda> agendaModel, final int colNum,
                                            final int rowNum) {
        return new PageCreator() {
            @Override
            public Page createPage() {
                return ChooseOptionsModalWindow.this.createPageInModalWindow(agendaModel, colNum, rowNum,
                        ChooseOptionsModalWindow.this);
            }
        };
    }

    protected abstract Page createPageInModalWindow(IModel<Agenda> agendaModel, int colNum,
                                                    int rowNum, ModalWindow modalWindow);
}