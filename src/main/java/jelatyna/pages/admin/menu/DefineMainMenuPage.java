package jelatyna.pages.admin.menu;

import jelatyna.domain.MainMenu;
import jelatyna.domain.MenuItem;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxButton;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;

@SuppressWarnings("serial")
public class DefineMainMenuPage extends DefineMenuBasePage {

    @Override
    protected MainMenu getMenu() {
        MainMenu menu = repository.getMainMenu();
        return menu != null ? menu : new MainMenu();
    }

    @Override
    void createManageItemButtons(String id, final ListItem<MenuItem> item) {
        super.createManageItemButtons(id, item);
        createAddSubitemButton(item);
        item.add(new ListView<MenuItem>("subitems", item.getModelObject().getChildren()) {
            @Override
            protected void populateItem(final ListItem<MenuItem> subitem) {
                DefineMainMenuPage.super.createManageItemButtons("subitem", subitem);
            }
        });
    }

    private MarkupContainer createAddSubitemButton(final ListItem<MenuItem> item) {
        return item.add(new AjaxButton("subitem_add", form) {

            @Override
            protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                item.getModelObject().addEmptyChild();
                target.add(form);
            }

            @Override
            protected void onError(AjaxRequestTarget target, Form<?> form) {

            }
        });
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CONFERENCE_SITE;
    }

    @Override
    public String getPageTitle() {
        return "Strona główna konferencji";
    }

}
