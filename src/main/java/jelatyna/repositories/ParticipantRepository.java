package jelatyna.repositories;

import jelatyna.domain.Participant;
import jelatyna.domain.RegistrationStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;

public interface ParticipantRepository extends JpaRepository<Participant, Integer> {

    @Query("FROM Participant WHERE lower(mail) = lower(?1)")
    Participant findByMail(String eMail);

    Participant findByToken(String token);

    @Query("FROM Participant WHERE lower(lastName) LIKE lower(?1)")
    List<Participant> findWithLastNameLike(String lastName);

    @Query("FROM Participant WHERE participated = true")
    List<Participant> findRealParticipants();

    @Query("SELECT DISTINCT upper(city) FROM Participant WHERE lower(city) LIKE lower(?1)")
    List<String> fetchCitiesStartingWith(String name);

    List<Participant> findByRegistrationTypeIn(Collection<RegistrationStatus> statuses);

}
