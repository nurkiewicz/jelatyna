package jelatyna.services;

import com.google.common.base.Joiner;
import jelatyna.domain.WithFile;
import org.apache.commons.collections.ListUtils;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.*;

@Service
public class FileService {
    private static final String SEPARATOR = "/";
    @Value("${files.folder}")
    String mainFolder;
    @Value("${files.context}")
    String context;
    @Autowired
    private FileSystem fileSystem;

    public void save(FileUpload fileUpload, List<String> folders) {
        if (fileUpload != null) {
            save(fileUpload, folders, fileUpload.getClientFileName());
        }
    }

    public void save(FileUpload fileUpload, WithFile withFile) {
        save(fileUpload, newArrayList(withFile.getSubfolder()), withFile.getFileName());
    }

    public void save(FileUpload fileUpload, List<String> folders, String fileName) {
        if (fileUpload != null) {
            fileSystem.saveFile(fileUpload, toPathInFolder(folders), fileName);
        }
    }

    public String getUrlTo(WithFile withFile) {
        if (withFile.getFileName() != null) {
            return getUrlTo(newArrayList(withFile.getSubfolder()), withFile.getFileName());
        } else {
            return "";
        }
    }

    public String getUrlTo(List<String> folderPath, String fileName) {
        folderPath.add(fileName);
        String server = getServer();
        return server + toPath(context, newArrayList(folderPath));
    }

    private String getServer() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) (RequestContextHolder.getRequestAttributes());
        if (attributes == null) {
            return "";
        }
        HttpServletRequest request = attributes.getRequest();
        return "http://c4p.confitura.pl";
    }

    @SuppressWarnings("unchecked")
    public List<java.io.File> listFilesIn(List<String> path) {
        File[] items = fileSystem.listFilesIn(toPathInFolder(path));
        List<File> folders = selectAndSort(items, true);
        List<File> files = selectAndSort(items, false);
        return ListUtils.union(folders, files);
    }

    public void newFolder(List<String> path, String newFolder) {
        fileSystem.createPath(toPath(path, newFolder));
    }

    public void delete(List<String> folders, String name) {
        fileSystem.delete(toPath(folders, name));
    }

    private String toPath(List<String> path, String name) {
        List<String> folders = newArrayList(path);
        folders.add(name);
        return toPathInFolder(folders);
    }

    private String toPathInFolder(List<String> folders) {
        return toPath(mainFolder, folders);
    }

    private String toPath(String main, List<String> folders) {
        return main + SEPARATOR + Joiner.on(SEPARATOR).join(folders);
    }


    private List<File> selectAndSort(File[] items, boolean isDir) {
        return Arrays.stream(items)
                .filter(i -> i.isDirectory() == isDir)
                .sorted(Comparator.comparing(File::getName))
                .collect(Collectors.toList());
    }

    public String getPathTo(WithFile withFile) {
        return toPath(newArrayList(withFile.getSubfolder()), withFile.getFileName());
    }

}
