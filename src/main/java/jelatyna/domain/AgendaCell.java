package jelatyna.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class AgendaCell extends AgendaItem {
    private static final long serialVersionUID = 8579255448233859949L;
    public int rowNum;
    public int colNum;

    public AgendaCell(int rowNum, int colNum, String value) {
        super(value);
        this.rowNum = rowNum;
        this.colNum = colNum;
    }

    public AgendaCell(int rowNum, int colNum, Integer presentationId) {
        super(presentationId);
        this.rowNum = rowNum;
        this.colNum = colNum;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof AgendaCell)) {
            return false;
        }
        AgendaCell other = (AgendaCell) o;
        return new EqualsBuilder()
                .append(colNum, other.colNum)
                .append(rowNum, other.rowNum)
                .append(presentationId, other.presentationId)
                .append(value, other.value)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(colNum)
                .append(rowNum)
                .append(presentationId)
                .append(value)
                .toHashCode();
    }

    public AgendaItem toAgendaItem() {
        return new AgendaItem(presentationId, value);
    }

}
