package jelatyna.api;


import static java.util.stream.Collectors.*;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.inject.internal.Maps;

import jelatyna.api.exceptions.EntityNotFoundException;
import jelatyna.api.exceptions.VoiceInvalidException;
import jelatyna.domain.Vote;
import jelatyna.domain.dto.VoteDto;
import jelatyna.domain.dto.VoteRequestDto;
import jelatyna.domain.dto.converters.VoteConverter;
import jelatyna.services.RandomGenerator;
import jelatyna.services.VotingService;

@Controller
public class VotingApi {


    @Autowired
    private RandomGenerator randomizeProvider;

    @Autowired
    private VotingService votingService;

    @Autowired
    private VoteConverter voteConverter;


    @RequestMapping(value = "v4p/{key}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getVotes(@PathVariable String key) {
        return asMap(key, votingService.getVotes(key));
    }

    private Map<String, Object> asMap(String key, List<Vote> votes) {
        Map<String, Object> map = Maps.newHashMap();
        map.put("key", key);
        map.put("presentations", toDto(votes));
        map.put("voted", votes.stream().filter((vote) -> {
            return vote.getRate() != null;
        }).count());
        return map;
    }

    @RequestMapping(value = "v4p", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> prepareVoting() {
        String key = randomizeProvider.getRandomId();
        return asMap(key, votingService.prepareVotesForKey(key));
    }


    @RequestMapping(value = "v4p/{key}", method = RequestMethod.POST)
    @ResponseBody
    public void vote(@PathVariable String key, @RequestBody VoteRequestDto request) {
        Vote vote = votingService.getVote(key, request.getPresentationId());
        validateVote(request.getRate(), vote);
        vote.setRate(request.getRate());
        votingService.save(vote);
    }

    private List<VoteDto> toDto(List<Vote> votes) {
        return votes.stream()
                .sorted((a, b) -> Integer.compare(a.getPositionInOrder(), b.getPositionInOrder()))
                .map(voteConverter::map)
                .collect(toList());
    }

    private void validateVote(Integer rate, Vote vote) {
        if (rate == null || rate < -1 || rate > 1) {
            throw new VoiceInvalidException();
        }
        if (vote == null) {
            throw new EntityNotFoundException();
        }
    }

}
