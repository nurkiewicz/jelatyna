package jelatyna.pages.admin.user;

import jelatyna.components.DeleteEntityLink;
import jelatyna.components.RedirectLink;
import jelatyna.domain.Admin;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.AdminService;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class ListAdminPage extends AdminBasePage {
    @SpringBean
    private AdminService service;

    public ListAdminPage() {
        add(new UserGrid(service.findAll()));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.ORGANIZERS;
    }

    @Override
    public String getPageTitle() {
        return AdminMenuItemEnum.ORGANIZERS.getLabel();
    }

    private final class UserGrid extends DataView<Admin> {
        private UserGrid(List<Admin> list) {
            super("rows", new ListDataProvider<Admin>(list));
        }

        @Override
        protected void populateItem(Item<Admin> item) {
            final Admin admin = item.getModelObject();
            item.add(label("firstName", admin.getFirstName()));
            item.add(label("lastName", admin.getLastName()));
            item.add(new RedirectLink("edit", AddAdminPage.class, admin));
            item.add(new RedirectLink("changePassword", ChangePasswordPage.class, admin));
            item.add(new DeleteEntityLink(admin, ListAdminPage.class) {
                @Override
                protected void deleteById(Integer id) {
                    service.deleteById(id);
                }
            });
        }
    }
}
