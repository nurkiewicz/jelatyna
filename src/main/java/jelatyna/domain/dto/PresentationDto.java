package jelatyna.domain.dto;

import java.util.List;

public class PresentationDto extends AbstractDto {
    private int id;
    private String title;
    private String shortDesc;
    private String longDesc;
    private List<UserDto> speakers;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getLongDesc() {
        return longDesc;
    }

    public void setLongDesc(String longDesc) {
        this.longDesc = longDesc;
    }

    public List<UserDto> getSpeakers() {
        return speakers;
    }

    public void setSpeakers(List<UserDto> speakers) {
        this.speakers = speakers;
    }

    public PresentationDto id(Integer id) {
        this.id = id;
        return this;
    }
}
