package jelatyna.pages.admin.registration;

import jelatyna.domain.RegistrationConfiguration;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.repositories.RegistrationConfigurationRepository;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.Radio;
import org.apache.wicket.markup.html.form.RadioGroup;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.List;

import static com.google.common.collect.Lists.*;
import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

@SuppressWarnings("serial")
public class RegistrationSettingsPage extends AdminBasePage {

    @SpringBean
    private RegistrationConfigurationRepository repository;

    public RegistrationSettingsPage() {
        add(new RegistrationForm(getRegistration()));
    }

    private RegistrationConfiguration getRegistration() {
        List<RegistrationConfiguration> all = repository.findAll();
        return all.isEmpty() ? new RegistrationConfiguration() : all.get(0);
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.PARTICIPANTS;
    }

    @Override
    public String getPageTitle() {
        return "Ustawienia rejestracji";
    }

    private final class RegistrationForm extends Form<Void> {
        private static final String CLOSE = "Zamknięta";
        private static final String ACTIVE = "Otwarta";
        private final RegistrationConfiguration registration;
        private String state;

        private RegistrationForm(RegistrationConfiguration registration) {
            super("form");
            this.registration = registration;
            state = registration.isActive() ? ACTIVE : CLOSE;
            RadioGroup<String> radioGroup = new RadioGroup<>("state", PropertyModel.<String>of(this, "state"));
            radioGroup.add(new ListView("options", new Model(newArrayList(ACTIVE, CLOSE))) {
                @Override
                protected void populateItem(ListItem component) {
                    Radio radio = new Radio("option", component.getModel());
                    radio.setLabel(Model.of(component.getDefaultModelObjectAsString()));
                    component.add(radio);
                }
            });
            add(radioGroup);
            add(richEditor("closeInfo", propertyModel(registration, "closeInfo")));
            add(richEditor("widget", propertyModel(registration, "widgetInfo")));
        }

        @Override
        protected void onSubmit() {
            registration.active(ACTIVE.equals(state));
            repository.save(registration);
        }

    }
}


