package jelatyna.pages.admin.news;

import jelatyna.TestApplicationWithSpringContextMock;
import jelatyna.domain.News;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class NewsPreviewLinkShould {

    private News news = mock(News.class);
    private WicketTester tester = new WicketTester(TestApplicationWithSpringContextMock.withoutInjector());

    @Test
    public void beDisabledWithProperLabelForNotPersistedNews() {
        // Given
        when(news.isNotNew()).thenReturn(false);

        // When
        NewsPreviewLink link = new NewsPreviewLink("anyId", news);
        tester.startComponent(link);

        // Then
        assertThat(link.isEnabled()).isFalse();
        assertThat(link.getLabel()).isEqualTo(NewsPreviewLink.LABEL_FOR_DISABLED);
    }

    @Test
    public void beEnabledWithProperLabelForPersistedNews() {
        // Given
        when(news.isNotNew()).thenReturn(true);

        // When
        NewsPreviewLink link = new NewsPreviewLink("anyId", news);
        tester.startComponent(link);

        // Then
        assertThat(link.isEnabled()).isTrue();
        assertThat(link.getLabel()).isEqualTo(NewsPreviewLink.LABEL_FOR_ENABLED);
    }
}
