package jelatyna.api;

import com.google.gson.Gson;
import jelatyna.domain.Participant;
import jelatyna.pages.confitura.registration.Registration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ContextConfiguration(locations = "classpath:test-spring.xml")
@WebAppConfiguration
public class RegistrationApiTest {

    public static final String PARTICIPANT_JSON = "{ \"firstName\" : \"Michal\", \"lastName\" : \"Karolik\"}";
    protected MockMvc mockMvc;

    @Mock
    private Registration registration;

    @InjectMocks
    private RegistrationApi target = new RegistrationApi();

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(target).build();
    }

    @Test
    public void returnCode200BecauseEverythingWentFine() throws Exception {

        doNothing().when(registration).register(any(Participant.class));

        mockMvc.perform(post("/api/register").content(PARTICIPANT_JSON).contentType(MediaType.APPLICATION_JSON)).
                andExpect(status().isOk());
    }

    @Test
    public void returnCode500BecauseRegistrationThrowError() throws Exception {

        doThrow(RuntimeException.class).when(registration).register(any(Participant.class));

        mockMvc.perform(post("/api/register").content(PARTICIPANT_JSON).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    }
}
