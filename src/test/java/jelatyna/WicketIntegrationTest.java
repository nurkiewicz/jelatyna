package jelatyna;

import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

abstract public class WicketIntegrationTest extends IntegrationTest implements ApplicationContextAware {
    @Autowired
    protected ApplicationContext applicationContext;

    protected WicketTester tester;

    @Before
    public void setUpWicketApplicationTester() {
        tester = new WicketTester(new TestApplication(applicationContext));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
