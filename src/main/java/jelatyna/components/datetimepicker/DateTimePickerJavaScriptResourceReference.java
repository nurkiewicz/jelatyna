package jelatyna.components.datetimepicker;

import org.apache.wicket.Application;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.HeaderItem;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

import java.util.Arrays;

public class DateTimePickerJavaScriptResourceReference extends JavaScriptResourceReference {

    private static DateTimePickerJavaScriptResourceReference INSTANCE = null;

    private DateTimePickerJavaScriptResourceReference() {
        super(DateTimePickerJavaScriptResourceReference.class, "datetimepicker.js");
    }

    public static DateTimePickerJavaScriptResourceReference get() {
        if (null == INSTANCE) {
            INSTANCE = new DateTimePickerJavaScriptResourceReference();
        }
        return INSTANCE;
    }

    @Override
    public Iterable<? extends HeaderItem> getDependencies() {
        return Arrays.asList(JavaScriptHeaderItem.forReference(Application.get().getJavaScriptLibrarySettings().getJQueryReference()),
                CssHeaderItem.forReference(DateTimePickerCssResourceReference.get()));
    }

    @Override
    public String getName() {
        return "datetimepicker.js";
    }

    @Override
    protected String getMinifiedName() {
        return "datetimepicker.js";
    }
}
