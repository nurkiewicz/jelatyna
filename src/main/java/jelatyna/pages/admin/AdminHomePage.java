package jelatyna.pages.admin;


import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;

import static jelatyna.pages.admin.headerpanel.AdminMenuItemEnum.*;

@SuppressWarnings("serial")
// @MountPath(path = "admin")
public class AdminHomePage extends AdminBasePage {

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return NONE;
    }

    @Override
    public String getPageTitle() {
        return "Panel administracyjny";
    }
}
