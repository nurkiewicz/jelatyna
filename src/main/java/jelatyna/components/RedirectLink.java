package jelatyna.components;

import jelatyna.domain.AbstractEntity;
import jelatyna.utils.Components;
import org.apache.wicket.Page;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.MarkupStream;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import static jelatyna.utils.PageParametersBuilder.*;
import static org.apache.commons.lang.StringUtils.*;

@SuppressWarnings("serial")
public class RedirectLink extends BookmarkablePageLink<Void> {

    private String anchor;
    private String label;

    public RedirectLink(String id, Class<? extends Page> page, AbstractEntity<?> entity) {
        this(id, page, paramsFor("id", entity.getId()));
    }

    public RedirectLink(String id, String labelKey, Class<? extends Page> page, AbstractEntity<?> entity) {
        this(id, page, paramsFor("id", entity.getId()));
        add(Components.i18nLabel(labelKey));
    }

    public RedirectLink(String id, Class<? extends Page> page, PageParameters params) {
        super(id, page, params);
    }

    public RedirectLink(String id, Class<? extends Page> page) {
        super(id, page);
    }

    public RedirectLink withAnchor(String anchor) {
        this.anchor = anchor;
        return this;
    }

    public RedirectLink withLabel(String label) {
        this.label = label;
        return this;
    }

    @Override
    protected CharSequence appendAnchor(ComponentTag tag, CharSequence url) {
        if (isNotBlank(anchor)) {
            url = url + "#" + anchor;
        }
        return super.appendAnchor(tag, url);
    }

    @Override
    public void onComponentTagBody(MarkupStream markupStream, ComponentTag openTag) {
        if (isNotBlank(label)) {
            replaceComponentTagBody(markupStream, openTag, label);
        } else {
            super.onComponentTagBody(markupStream, openTag);
        }
    }

}