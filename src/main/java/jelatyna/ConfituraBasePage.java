package jelatyna;

import jelatyna.pages.PageHeader;
import org.apache.wicket.Page;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.ResourceModel;

import java.util.Collection;

import static jelatyna.utils.PageParametersBuilder.*;

@SuppressWarnings("serial")
public class ConfituraBasePage extends WebPage {

    private PageHeader pageHeader = new PageHeader(
            getString("default.page.title.prefix"),
            getString("default.description.for.facebook"),
            getString("conference.logo.image.for.facebook"));

    @Override
    public ConfituraSession getSession() {
        return ConfituraSession.get();
    }

    protected void setResponse(Class<? extends Page> page, Object id) {
        setResponsePage(page, paramsFor("id", id));
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        pageHeader.renderHead(response);
    }

    public void setPageTitlePostfix(ResourceModel model) {
        pageHeader.setPageTitlePostfix(model.getObject());
    }

    public void setPageTitlePostfix(String titlePostfix) {
        pageHeader.setPageTitlePostfix(titlePostfix);
    }

    public void setDescriptionForFacebook(String descriptionForFacebook) {
        pageHeader.setDescriptionForFacebook(descriptionForFacebook);
    }

    public void addImageForFacebook(String imageUrl) {
        pageHeader.addImageForFacebook(imageUrl);
    }

    public void addImagesForFacebook(Collection<String> imageUrls) {
        imageUrls.forEach(this::addImageForFacebook);
    }
}
