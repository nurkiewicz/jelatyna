package jelatyna.repositories;

import jelatyna.domain.Agenda;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AgendaRepository {
    @Autowired
    private AgendaEntityRepository agendaEntityRepository;

    public Agenda getAgenda() {
        AgendaEntity agendaEntity = agendaEntityRepository.findOne(AgendaEntity.AGENDA_KEY);
        if (agendaEntity == null) {
            return new Agenda();
        }
        return agendaEntity.getAgenda();
    }

    public void saveAgenda(Agenda agenda) {
        agendaEntityRepository.save(new AgendaEntity(agenda));
    }
}
