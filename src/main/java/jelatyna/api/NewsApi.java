package jelatyna.api;

import jelatyna.domain.dto.NewsDto;
import jelatyna.services.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class NewsApi {
    @Autowired
    private NewsService service;

    @RequestMapping(value = "/news/{page}/{pageSize}", method = RequestMethod.GET)
    @ResponseBody
    public List<NewsDto> getNewsPage(@PathVariable int page, @PathVariable int pageSize) {
        return service.findPublished(page, pageSize).getContent()
                .stream()
                .map((news) -> {
                    return new NewsDto()
                            .title(news.getTitle())
                            .author(news.getAutor().getFullName())
                            .date(new DateApi(news.creationDateAsLocalDateTime()))
                            .shortText(news.getShortDescription())
                            .text(news.getDescription());
                })
                .collect(Collectors.toList());
    }
}
