package jelatyna.pages.admin.c4p;

import jelatyna.components.mail.DefineMailPanel;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;

@SuppressWarnings("serial")
public class DefineResetPassMailPage extends AdminBasePage {

    public DefineResetPassMailPage() {
        add(new DefineMailPanel("mail", "speakerResetPass", "$firstName, $lastName, $url"));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CALL_4_PAPERS;
    }

    @Override
    public String getPageTitle() {
        return "Reset Password E-mail";
    }
}
