/**
 * @name Shuffle Letters
 * @author Martin Angelov
 * @version 1.0
 * @url http://tutorialzine.com/2011/09/shuffle-letters-effect-jquery/
 * @license MIT License
 */

(function ($) {

    $.fn.shuffleLetters = function (prop) {

        // Handling default arguments
        var options = $.extend({
            // Default arguments
        }, prop)

        return this.each(function () {
            // The main plugin code goes here
        });
    };

    // A helper function

    function randomChar() {
        // Generate and return a random character
    }

})(jQuery);

function randomChar() {
    var pool = "";

    pool = ",.?/\\(^)![]{}*&^%$#'\"abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    var arr = pool.split('');
    return arr[Math.floor(Math.random() * arr.length)];
}

$.fn.shuffleLetters = function (prop) {

    var options = $.extend({
        "step": 20,	// How many times should the letters be changed
        "fps": 25,	// Frames Per Second
        "text": "" 	// Use this text instead of the contents
    }, prop)

    return this.each(function () {

        var el = $(this),
            str = "";

        if (options.text) {
            str = options.text.split('');
        }
        else {
            str = el.text().split('');
        }

        // Letters holds the positions of non-space characters;

        var letters = [];

        // Looping through all the chars of the string

        for (var i = 0; i < str.length; i++) {

            var ch = str[i];

            letters.push(i);
        }

        el.html("");

        // Self executing named function expression:

        (function shuffle(start) {

            // This code is run options.fps times per second
            // and updates the contents of the page element

            var i,
                len = letters.length,
                strCopy = str.slice(0);	// Fresh copy of the string

            if (start > len) {
                return;
            }

            // All the work gets done here
            for (i = Math.max(start, 0); i < len; i++) {

                // The start argument and options.step limit
                // the characters we will be working on at once

                if (i < start + options.step) {
                    // Generate a random character at this position
                    strCopy[letters[i]] = randomChar();
                }
                else {
                    strCopy[letters[i]] = "";
                }
            }

            el.text(strCopy.join(""));

            setTimeout(function () {

                shuffle(start + 1);

            }, 1000 / options.fps);

        })(-options.step);

    });

};
