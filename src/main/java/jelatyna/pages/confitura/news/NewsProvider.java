package jelatyna.pages.confitura.news;

import jelatyna.domain.News;
import jelatyna.services.NewsService;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.Iterator;

@SuppressWarnings("serial")
public class NewsProvider implements IDataProvider<News> {

    private final NewsService service;

    public NewsProvider(NewsService service) {
        this.service = service;
    }

    @Override
    public void detach() {
    }

    @Override
    public long size() {
        return service.countPublished().intValue();
    }

    @Override
    public IModel<News> model(News news) {
        return new Model<>(news);
    }

    @Override
    public Iterator<? extends News> iterator(long first, long count) {
        return service.findPublished((int) (first / count), (int) count).iterator();
    }
}
