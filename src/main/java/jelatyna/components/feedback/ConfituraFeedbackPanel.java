package jelatyna.components.feedback;

import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.feedback.IFeedbackMessageFilter;
import org.apache.wicket.markup.html.panel.FeedbackPanel;

@SuppressWarnings("serial")
public class ConfituraFeedbackPanel extends FeedbackPanel {

    private static String INFO_CLASS = "alert alert-success";
    private static String WARN_CLASS = "alert alert-info";
    private static String ERROR_CLASS = "alert alert-error";

    public ConfituraFeedbackPanel(String id) {
        super(id);
    }

    public ConfituraFeedbackPanel(final String id, IFeedbackMessageFilter filter) {
        super(id, filter);
    }

    @Override
    protected String getCSSClass(final FeedbackMessage message) {
        switch (message.getLevel()) {
            case FeedbackMessage.ERROR:
            case FeedbackMessage.FATAL:
                return ERROR_CLASS;

            case FeedbackMessage.DEBUG:
            case FeedbackMessage.UNDEFINED:
            case FeedbackMessage.WARNING:
                return WARN_CLASS;

            case FeedbackMessage.INFO:
            case FeedbackMessage.SUCCESS:
                return INFO_CLASS;

            default:
                return WARN_CLASS;
        }
    }
}
