package jelatyna.components.user;

import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.domain.User;
import jelatyna.services.UserService;
import jelatyna.services.exceptions.LoginException;
import org.apache.wicket.Page;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.validation.EqualPasswordInputValidator;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.ResourceModel;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;

@SuppressWarnings("serial")
public class ChangePasswordPanel<USER extends User<?>> extends Panel {

    private final USER user;
    private final UserService<USER> service;
    private final Class<? extends Page> redirectPage;
    private PasswordTextField oldPassword = withLabelKey("password.old.label", passwordField("oldPassword", model(), true));
    private PasswordTextField password = withLabelKey("password.new.label", passwordField("password", model(), true));
    private PasswordTextField repassword = withLabelKey("password.new.repeat.label", passwordField("repassword", model(), true));

    public ChangePasswordPanel(USER user, UserService<USER> service, Class<? extends Page> redirectPage) {
        super("changePasswordPanel");
        this.user = user;
        this.service = service;
        this.redirectPage = redirectPage;
        add(new ChangePasswordForm());
        add(i18nLabel("password.change.label"));
    }

    protected void doChangePassword() {
        try {
            service.changePassword(user, oldPassword.getValue(), password.getValue());
            setResponsePage(redirectPage);
        } catch (LoginException ex) {
            warn(new ResourceModel(ex.getMessage()).getObject());
        }
    }

    private final class ChangePasswordForm extends Form<Void> {
        public ChangePasswordForm() {
            super("form");
            add(new ConfituraFeedbackPanel("feedback"));
            add(i18nLabel("password.old.label"), oldPassword);
            add(i18nLabel("password.new.label"), password);
            add(i18nLabel("password.new.repeat.label"), repassword);
            add(cancelLinkWithLabel(redirectPage), i18nLabel("save.label"));
            add(new EqualPasswordInputValidator(password, repassword));

        }

        @Override
        protected void onSubmit() {
            doChangePassword();
        }
    }
}