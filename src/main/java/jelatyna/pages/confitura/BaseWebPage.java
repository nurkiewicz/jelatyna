package jelatyna.pages.confitura;

import jelatyna.ConfituraBasePage;
import jelatyna.ConfituraSession;
import jelatyna.components.menu.MenuLinks;
import jelatyna.repositories.MenuRepository;
import jelatyna.security.SecurityUtils;
import jelatyna.services.SponsorService;
import org.apache.commons.lang.StringUtils;
import org.apache.wicket.Application;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.PriorityHeaderItem;
import org.apache.wicket.spring.injection.annot.SpringBean;

@SuppressWarnings("serial")
public class BaseWebPage extends ConfituraBasePage {
    @SpringBean
    private MenuRepository menuRepository;
    @SpringBean
    private MenuLinks menuItemList;
    @SpringBean
    private SponsorService sponsorService;

    public BaseWebPage() {
//        add(new MainMenuPanel("mainMenu", menuRepository, menuItemList));
//        add(new SponsorWidget("main_sponsors", sponsorService.getMainTypes()));
//        add(new SponsorWidget("sponsors", sponsorService.getOtherTypes()));
//        add(new SpeakersWidget("speaker_widget"));
    }


    private String getGreetingLabel() {
        String username = SecurityUtils.getUsername();
        if (StringUtils.isNotBlank(username) && !SecurityUtils.isAnonymous()) {
            return "Witaj, " + username;
        } else {
            return "";
        }
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        response.render(new PriorityHeaderItem(JavaScriptHeaderItem.forReference(Application.get().getJavaScriptLibrarySettings()
                .getJQueryReference())));
        super.renderHead(response);
    }

    @Override
    public ConfituraSession getSession() {
        return ConfituraSession.get();
    }

}
