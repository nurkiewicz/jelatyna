package jelatyna.pages.volunteer;

import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.pages.admin.registration.RegisterParticipantsPanel;

@SuppressWarnings("serial")
public class VolunteerHomePage extends VolunteerBasePage {

    public VolunteerHomePage() {
        add(new RegisterParticipantsPanel("RegisterPanel"));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.NONE;
    }

    @Override
    public String getPageTitle() {
        return "Wolontariusz";
    }
}
