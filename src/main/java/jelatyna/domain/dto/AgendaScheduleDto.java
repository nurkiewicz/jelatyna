package jelatyna.domain.dto;

import java.util.List;

public class AgendaScheduleDto extends AbstractDto {
    private int slotId;
    private List<AgendaSlotDto> presentations;

    public int getSlotId() {
        return slotId;
    }

    public void setSlotId(int slotId) {
        this.slotId = slotId;
    }

    public List<AgendaSlotDto> getPresentations() {
        return presentations;
    }

    public void setPresentations(List<AgendaSlotDto> presentations) {
        this.presentations = presentations;
    }

    public AgendaScheduleDto slotId(final int slotId) {
        this.slotId = slotId;
        return this;
    }

    public AgendaScheduleDto presentations(final List<AgendaSlotDto> presentations) {
        this.presentations = presentations;
        return this;
    }

}
