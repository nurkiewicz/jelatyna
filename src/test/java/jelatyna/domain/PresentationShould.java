package jelatyna.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class PresentationShould {
    private Presentation presentation = new Presentation();

    @Test
    public void getStatusKeyIfNotAccepted() {
        presentation.accepted(false);

        String key = presentation.getStatusKey();

        assertThat(key).isEqualTo("presentation.status.new");
    }

    @Test
    public void getStatusKeyIfAccepted() {
        presentation.accepted(true);

        String key = presentation.getStatusKey();

        assertThat(key).isEqualTo("presentation.status.accepted");
    }

    @Test
    public void be_own_by_the_owner() {
        Speaker owner = new Speaker(1);
        presentation.owner(owner);

        boolean notOwned = presentation.notOwnedBy(owner);

        assertThat(notOwned).isFalse();
    }

    @Test
    public void not_be_own_by_other_speaker() {
        Speaker speaker = new Speaker(2);
        presentation.owner(new Speaker(1)).addSpeaker(speaker);

        boolean notOwned = presentation.notOwnedBy(speaker);

        assertThat(notOwned).isTrue();
    }


}
