package jelatyna.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.Entity;

@SuppressWarnings("serial")
@Entity
public class SponsorType extends AbstractEntity<SponsorType> {
    public static final SponsorType EMPTY = new SponsorType("EMPTY");
    private String name;
    private Integer minimalNo;
    private Integer position;
    private String logoPlaceholder;
    private String urlPlaceholder;

    public SponsorType() {
    }

    public SponsorType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getMinimalNo() {
        return minimalNo;
    }

    public SponsorType position(int position) {
        this.position = position;
        return this;
    }

    public boolean isNotFirst() {
        return position != 0;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "SponsorType [name=" + name + ", minimalNo=" + minimalNo + ", order=" + position + "]";
    }

    public SponsorType minimalNo(int minimalNo) {
        this.minimalNo = minimalNo;
        return this;
    }

    public boolean shouldDisplayPlaceholders() {
        return minimalNo > 0;
    }

    public String getLogoPlaceholder() {
        return logoPlaceholder;
    }

    public String getUrlPlaceholder() {
        return urlPlaceholder;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SponsorType == false) {
            return false;
        }
        SponsorType other = (SponsorType) obj;
        return new EqualsBuilder()
                .append(getId(), other.getId())
                .append(name, other.name)
                .append(position, other.position)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(getId())
                .append(name)
                .append(position)
                .toHashCode();
    }

    public SponsorType logoPlaceholder(String logoPlaceholder) {
        this.logoPlaceholder = logoPlaceholder;
        return this;
    }

    public SponsorType urlPlaceholder(String urlPlaceholder) {
        this.urlPlaceholder = urlPlaceholder;
        return this;
    }

}
