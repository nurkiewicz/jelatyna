package jelatyna.api;

import jelatyna.domain.AgendaCell;
import jelatyna.domain.dto.*;
import jelatyna.repositories.AgendaRepository;
import jelatyna.services.PresentationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

@Controller
public class AgendaApi {


    @Autowired
    AgendaRepository agendaRepository;
    @Autowired
    PresentationService service;

    @RequestMapping(value = "agenda", method = RequestMethod.GET)
    @ResponseBody
    public AgendaDto agenda() {
        AgendaCell[][] matrix = agendaRepository.getAgenda().getMatrix();

        return new AgendaDto()
                .rooms(collectRooms(matrix))
                .slots(collectSlots(matrix))
                .schedule(collectSchedule(matrix));
    }


    private List<String> collectRooms(AgendaCell[][] a) {
        return asList(a[0])
                .stream().skip(1)
                .map(AgendaCell::getValue)
                .collect(toList());
    }
    private List<TimeSlotDto> collectSlots(AgendaCell[][] matrix) {
        return asList(matrix)
                .stream().skip(1)
                .map(s -> slot(s[0]))
                .collect(toList());
    }
    private List<AgendaScheduleDto> collectSchedule(AgendaCell[][] matrix) {
        return asList(matrix).stream().skip(1)
                .map((items) -> new AgendaScheduleDto()
                        .slotId(items[0].rowNum)
                        .presentations(asList(items)
                                .stream().skip(1)
                                .map(this::presentations)
                                .collect(toList())))
                .collect(toList());
    }

    private TimeSlotDto slot(AgendaCell s) {
        String[] time = s.value.split("-");
        return new TimeSlotDto()
                .id(s.rowNum)
                .start(time(time[0]))
                .end(time(time[1]));
    }

    private String time(String s) {
        return s.length() == 5 ? s : s + ":00";
    }

    private AgendaSlotDto presentations(AgendaCell item) {
        return new AgendaSlotDto()
                .room(agendaRepository.getAgenda().getRooms().get(item.colNum))
                .presentation(new AgendaPresentationDto().title(getTitle(item)));
    }

    private String getTitle(AgendaCell item) {
        return item.hasPresentation() ?
                service.findBy(item.presentationId).getTitle() :
                item.getValue();
    }
}
