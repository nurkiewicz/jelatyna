package jelatyna.mounter;

import jelatyna.Confitura;
import jelatyna.pages.PageNotFound;
import jelatyna.pages.confitura.ViewSimpleContentPage;
import jelatyna.pages.confitura.agenda.AgendaICalendarPage;
import jelatyna.pages.confitura.agenda.AgendaPresentationPage;
import jelatyna.pages.confitura.c4p.ChangePasswordPage;
import jelatyna.pages.confitura.c4p.EditSpeakerPage;
import jelatyna.pages.confitura.c4p.RegisterSpeakerPage;
import jelatyna.pages.confitura.c4p.ViewSpeakerPage;
import jelatyna.pages.confitura.c4p.login.LoginSpeakerPage;
import jelatyna.pages.confitura.c4p.login.RequestPasswordResetPage;
import jelatyna.pages.confitura.c4p.login.ResetPasswordPage;
import jelatyna.pages.confitura.c4p.presentation.AddPresentationPage;
import jelatyna.pages.confitura.c4p.voting.VotingFinishedPage;
import jelatyna.pages.confitura.c4p.voting.VotingPage;
import jelatyna.pages.confitura.news.ListNewsPage;
import jelatyna.pages.confitura.news.NewsDetailsPage;
import jelatyna.pages.confitura.registration.RegistrationInfoPage;
import jelatyna.pages.confitura.registration.form.RegistrationPage;
import jelatyna.pages.confitura.registration.process.CancelRegistrationPage;
import jelatyna.pages.confitura.registration.process.ConfirmRegistrationPage;
import jelatyna.pages.confitura.registration.process.FinallyConfirmRegistrationPage;
import jelatyna.pages.confitura.speaker.ListPresentationPage;

import static java.lang.String.*;
import static jelatyna.Confitura.*;
import static jelatyna.utils.PageParametersBuilder.*;

public class StandardModuleMounter extends ModuleMounter {

    public StandardModuleMounter(Confitura confitura) {
        super("", confitura);
    }

    @Override
    public void mount() {
//        news();
//        staticPages();
//        mountPage("/kapitula", ViewAdminPage.class);
//        mountPage("/sponsors", ViewSponsorsPage.class);
//        mountPage("/speakers", ListSpeakerPage.class);
//        mountPage("/volunteers", ListVolunteerPage.class);
        mountPage("/presentations", ListPresentationPage.class);
//        agenda();
        c4p();
//        registration();
        mountPage("/404", PageNotFound.class);
    }

    void agenda() {
        mountPage("/agenda", AgendaPresentationPage.class);
        mountPage("/agenda.ics", AgendaICalendarPage.class);
    }

    void staticPages() {
        mountPage("/view/${id}", ViewSimpleContentPage.class);
        mountPage("/view/${id}/${title}", ViewSimpleContentPage.class);
    }

    void news() {
        mountPage(NEWS_BASE_URL, ListNewsPage.class);
        mountPage(NEWS_BASE_URL + format("/${%s}", TITLE), NewsDetailsPage.class);
        mountPage(NEWS_BASE_URL + format("/${%s}/${%s}", ID, TITLE), NewsDetailsPage.class);
    }

    void c4p() {
        mountPage("/c4p/login", LoginSpeakerPage.class);
        mountPage("/c4p/speaker/register", RegisterSpeakerPage.class);
        mountPage("/c4p/speaker/view", ViewSpeakerPage.class);
        mountPage("/c4p/speaker/edit", EditSpeakerPage.class);
        mountPage("/c4p/speaker/add", AddPresentationPage.class);
        mountPage("/c4p/password/reset/${token}", ResetPasswordPage.class);
        mountPage("/c4p/password/reset", RequestPasswordResetPage.class);
        mountPage("/c4p/password/change", ChangePasswordPage.class);
        mountPage("/c4p/voting/${id}", VotingPage.class);
        mountPage("/c4p/voting/finish", VotingFinishedPage.class);
    }

    void registration() {
        mountPage("/registration", RegistrationPage.class);
        mountPage("/registration/info", RegistrationInfoPage.class);
        mountPage("/registration/confirm/${token}", ConfirmRegistrationPage.class);
        mountPage("/registration/cancel/${token}", CancelRegistrationPage.class);
        mountPage("/registration/final/${token}", FinallyConfirmRegistrationPage.class);
    }

}
