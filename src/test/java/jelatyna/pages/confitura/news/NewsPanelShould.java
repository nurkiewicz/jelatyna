package jelatyna.pages.confitura.news;

import jelatyna.WicketTest;
import jelatyna.domain.Admin;
import jelatyna.domain.News;
import jelatyna.pages.confitura.ViewAdminPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.util.tester.TagTester;
import org.junit.Test;

import static jelatyna.TestUtils.*;
import static jelatyna.utils.PageParametersBuilder.*;
import static org.assertj.core.api.Assertions.*;

public class NewsPanelShould extends WicketTest {
    private News news = new News()
            .id(1)
            .title("News Title")
            .shortDescription("short description").description("full description")
            .autor(admin());

    @Test
    public void showFullNews() {

        tester.startComponentInPage(new NewsPanel("panel", news, true));

        asertTitleLinkContains(news);
        tester.assertLabel("panel:shortDescription", news.getShortDescription());
        tester.assertLabel("panel:description", news.getDescription());
        tester.assertInvisible("panel:more");
        assertAuthorLinkContains(news.getAutor());
    }

    @Test
    public void showSiplifyNews() {

        tester.startComponentInPage(new NewsPanel("panel", news, false));

        asertTitleLinkContains(news);
        tester.assertLabel("panel:shortDescription", news.getShortDescription());
        tester.assertInvisible("panel:description");
        assertAuthorLinkContains(news.getAutor());
        assertMoreLinkRendered();
    }

    @Test
    public void showNotRenderMoreLinkForSimplifyNewsIfDescriptionIsEmpty() {
        news.description("");

        tester.startComponentInPage(new NewsPanel("panel", news, true));

        tester.assertInvisible("panel:more");
    }

    private void assertMoreLinkRendered() {
        tester.assertBookmarkablePageLink("panel:more", NewsDetailsPage.class,
                params().id(news.getId()).title("news_title").build());
    }

    private void asertTitleLinkContains(News news) {
        tester.assertBookmarkablePageLink("panel:title", NewsDetailsPage.class,
                params().id(news.getId()).title("news_title").build());
        assertThat(tester.getTagByWicketId("title").getValue()).isEqualTo(news.getTitle());
    }

    private void assertAuthorLinkContains(Admin author) {
        tester.assertBookmarkablePageLink("panel:author", ViewAdminPage.class, new PageParameters());
        TagTester tagTester = tester.getTagByWicketId("author");
        assertThat(tagTester.getAttribute("href")).endsWith("#" + author.getFullName());
        assertThat(tagTester.getValue()).endsWith(author.getFullName());
    }
}
