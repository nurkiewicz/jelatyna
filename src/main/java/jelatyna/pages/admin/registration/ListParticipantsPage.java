package jelatyna.pages.admin.registration;

import com.google.gson.Gson;
import jelatyna.components.RedirectLink;
import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.domain.Participant;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.graphs.Graph;
import jelatyna.pages.admin.graphs.Graphs;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.repositories.ParticipantRepository;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.data.domain.Sort;

import java.util.List;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.DateUtils.*;

@SuppressWarnings("serial")
public class ListParticipantsPage extends AdminBasePage {

    @SpringBean
    private ParticipantRepository repository;

    public ListParticipantsPage() {
        add(new ConfituraFeedbackPanel("feedback"));
        add(new BookmarkablePageLink<EditParticipantPage>("addParticipantLink", EditParticipantPage.class));

        List<Participant> participants = repository.findAll(new Sort("registrationDate"));
        Graph graph = new Graphs(participants).registrationGraph();
        add(graph);
        add(new ParticipantsList(participants));
    }

    private void add(Graph graph) {
        add(label("xAxis", new Gson().toJson(graph.getXAxis())).setEscapeModelStrings(false));
        add(label("yAxis", new Gson().toJson(graph.getYAxis())).setEscapeModelStrings(false));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.PARTICIPANTS;
    }

    @Override
    public String getPageTitle() {
        return "Zarejestrowani uczestnicy";
    }

    private final class ParticipantsList extends ListView<Participant> {
        private ParticipantsList(List<? extends Participant> list) {
            super("rows", list);
        }

        @Override
        protected void populateItem(ListItem<Participant> item) {
            Participant participant = item.getModelObject();
            CompoundPropertyModel<Participant> model = new CompoundPropertyModel<Participant>(participant);
            item.setModel(model);
            item.add(label("id", (item.getIndex() + 1) + ""));
            item.add(label("firstName"));
            item.add(label("lastName"));
            item.add(label("mail"));
            item.add(label("city"));
            item.add(label("sex"));
            item.add(label("status"));
            item.add(label("time", participant.getRegistrationTime().toString(TIME_FORMAT)));
            item.add(new RedirectLink("editUserLink", EditParticipantPage.class, participant));
        }
    }
}
