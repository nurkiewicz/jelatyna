package jelatyna.api;

import jelatyna.domain.Presentation;
import jelatyna.domain.dto.PresentationDto;
import jelatyna.domain.dto.converters.PresentationConverter;
import jelatyna.services.PresentationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import static java.util.Arrays.*;
import static java.util.Collections.*;
import static org.fest.assertions.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PresentationApiShould {
    private final Presentation PRESENTATION_1 = new Presentation().id(10);
    private final Presentation PRESENTATION_2 = new Presentation().id(20);
    private final List<Presentation> SAMPLE_PRESENTATIONS = asList(PRESENTATION_1, PRESENTATION_2);

    @Mock
    private PresentationService presentationService;

    @Mock
    private PresentationConverter presentationConverter;
    @InjectMocks
    private PresentationApi api = new PresentationApi();


    @Test
    public void return_converted_presentation() {
        when(presentationService.findBy(10)).thenReturn(PRESENTATION_1);
        PresentationDto dto = new PresentationDto();
        when(presentationConverter.map(any(Presentation.class))).thenReturn(dto);

        PresentationDto result = api.getPresentation(10);

        assertThat(result).isSameAs(dto);
        verify(presentationConverter).map(PRESENTATION_1);
    }

    @Test
    public void returnNullWhenServiceReturnsNull() {
        when(presentationService.findBy(any())).thenReturn(null);

        PresentationDto result = api.getPresentation(10);

        assertThat(result).isNull();
    }

    @Test
    public void returnEmptyListWhenNoAcceptedPresentationIsAvailable() {
        when(presentationService.acceptedPresentations()).thenReturn(EMPTY_LIST);

        List<PresentationDto> result = api.acceptedPresentations();

        assertThat(result).hasSize(0);
    }

    @Test
    public void returnAcceptedPresentations() {
        PresentationDto dto1 = new PresentationDto().id(1);
        PresentationDto dto2 = new PresentationDto().id(2);
        when(presentationService.acceptedPresentations()).thenReturn(SAMPLE_PRESENTATIONS);
        when(presentationConverter.map(PRESENTATION_1)).thenReturn(dto1);
        when(presentationConverter.map(PRESENTATION_2)).thenReturn(dto2);

        List<PresentationDto> dtos = api.acceptedPresentations();

        assertThat(dtos).containsOnly(dto1, dto2);
    }


}
