package jelatyna.pages.admin.registration;

import jelatyna.domain.Participant;
import jelatyna.pages.confitura.registration.process.CancelRegistrationPage;
import jelatyna.pages.confitura.registration.process.FinallyConfirmRegistrationPage;
import jelatyna.services.AbsoluteUrlProvider;
import jelatyna.utils.MailSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.google.common.collect.Lists.*;
import static jelatyna.TestUtils.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GroupMailSenderShould {
    @Mock
    private MailSender mailSender;
    @Mock
    private AbsoluteUrlProvider urlProvider;
    @InjectMocks
    private GroupMailSender groupSender = new GroupMailSender(0);

    @Test
    public void sendSetAllVariablesAndSendMessagesParticipant() {
        String token = "1";
        Participant participant = participant("Jan", "Kowalski").token(token);
        String confirmLink = "confirm_url";
        String cancelLink = "cancel_url";
        mockConfirmLink(confirmLink);
        mockCancelLink(cancelLink);

        groupSender.sendMessages(newArrayList(participant));

        InOrder inOrder = inOrder(mailSender);
        inOrder.verify(mailSender).set("firstName", participant.getFirstName());
        inOrder.verify(mailSender).set("lastName", participant.getLastName());
        inOrder.verify(mailSender).set("confirmLink", confirmLink + token);
        inOrder.verify(mailSender).set("cancelLink", cancelLink + token);
        inOrder.verify(mailSender).sendMessage(participant);
    }

    @Test
    public void sendMessagesToAllParticipants() {
        Participant participant1 = participant(1).token("1");
        Participant participant2 = participant(2).token("2");

        groupSender.sendMessages(newArrayList(participant1, participant2));

        verify(mailSender).sendMessage(participant1);
        verify(mailSender).sendMessage(participant2);
    }

    private void mockCancelLink(String cancelUrl) {
        when(urlProvider.getPathFor(CancelRegistrationPage.class)).thenReturn(cancelUrl);
    }

    private void mockConfirmLink(String confirmUrl) {
        when(urlProvider.getPathFor(FinallyConfirmRegistrationPage.class)).thenReturn(confirmUrl);
    }
}
