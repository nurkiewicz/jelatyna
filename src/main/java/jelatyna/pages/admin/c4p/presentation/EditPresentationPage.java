package jelatyna.pages.admin.c4p.presentation;

import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.pages.admin.AbstractAddPage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.pages.confitura.c4p.presentation.PresentationPanel;
import jelatyna.services.PresentationService;
import jelatyna.services.SpeakerService;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.StringValue;

@SuppressWarnings("serial")
public class EditPresentationPage extends AbstractAddPage<Presentation> {
    @SpringBean
    private PresentationService service;
    @SpringBean
    private SpeakerService speakerService;
    private Speaker speaker;

    public EditPresentationPage(PageParameters params) {
        super(params);
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CALL_4_PAPERS;
    }

    @Override
    public String getPageTitle() {
        return getActiveMenu().getLabel();
    }

    @Override
    protected Presentation findById(int id) {
        return service.findBy(id);
    }

    @Override
    protected Presentation createNew() {
        Presentation presentation = new Presentation();
        speaker.addPresentation(presentation);
        return presentation;
    }

    @Override
    protected void initPage(Presentation entity) {
        add(PresentationPanel.forAdmin("presentation", entity));
    }

    @Override
    protected void handleAdditionalParameters(PageParameters params) {
        StringValue speakerId = params.get("speaker_id");
        speaker = speakerId.isEmpty() ? null : speakerService.findById(speakerId.toInt());
    }
}
