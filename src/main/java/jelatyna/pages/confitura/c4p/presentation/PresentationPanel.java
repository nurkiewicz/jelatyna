package jelatyna.pages.confitura.c4p.presentation;

import jelatyna.components.feedback.ConfituraFeedbackPanel;
import jelatyna.components.nogeneric.Link;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.pages.admin.c4p.presentation.ListPresentationPage;
import jelatyna.pages.confitura.c4p.ViewSpeakerPage;
import jelatyna.services.PresentationService;
import jelatyna.services.SpeakerService;
import org.apache.wicket.Page;
import org.apache.wicket.extensions.validation.validator.RfcCompliantEmailAddressValidator;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.Radio;
import org.apache.wicket.markup.html.form.RadioGroup;
import org.apache.wicket.markup.html.form.SubmitLink;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.domain.Presentation.*;
import static jelatyna.utils.Components.*;
import static jelatyna.utils.Models.*;
import static org.apache.wicket.validation.validator.StringValidator.*;

@SuppressWarnings("serial")
public class PresentationPanel extends Panel {
    @SpringBean
    private PresentationService service;
    @SpringBean
    private SpeakerService speakerService;

    private boolean forAdmin;
    private Class<? extends Page> returnPage;

    private PresentationPanel(String id, Presentation presentation, boolean forAdmin, Class<? extends Page> returnPage) {
        super(id);
        this.forAdmin = forAdmin;
        this.returnPage = returnPage;

        add(new PresentationForm(presentation));
        add(i18nLabel("presentation.desc.short.info", new StringResourceModel("presentation.desc.short.info", null, SHORT_DESC_MAX_LENGTH)));
        add(label("limit", SHORT_DESC_MAX_LENGTH));
    }

    public static PresentationPanel forPresenter(String id, Presentation presentation) {
        return new PresentationPanel(id, presentation, false, ViewSpeakerPage.class);
    }

    public static PresentationPanel forAdmin(String id, Presentation presentation) {
        return new PresentationPanel(id, presentation, true, ListPresentationPage.class);
    }

    private final class PresentationForm extends Form<Presentation> {
        private final FileUploadField fileUpload = new FileUploadField("file");
        private final Presentation presentation;

        private PresentationForm(Presentation presentation) {
            super("form");
            this.presentation = presentation;
            add(new ConfituraFeedbackPanel("feedback"));
            add(i18nLabel("presentation.title.label"),
                    withLabelKey("presentation.title.label", textField("title", propertyModel(presentation, "title"), true)));
            add(i18nLabel("presentation.language.label"), languageRadios(presentation));
            add(i18nLabel("presentation.file.label"), fileUpload);
            add(moviesLinks(presentation));
            add(i18nLabel("presentation.desc.short.label"),
                    withLabelKey("presentation.desc.short.label",
                            textArea("shortDescription", propertyModel(presentation, "shortDescription"))).add(
                            maximumLength(SHORT_DESC_MAX_LENGTH))
            );
            add(i18nLabel("presentation.desc.label"),
                    withLabelKey("presentation.desc.label", richEditor("description", propertyModel(presentation, "description"))));
            add(cancelLinkWithLabel(returnPage));
            add(i18nLabel("save.label"));

            add(new SpeakersList("speakers"));
            add(new AddSpeakerForm("addSpeakerForm"));
        }

        private RadioGroup<String> languageRadios(Presentation presentation) {
            RadioGroup<String> group = new RadioGroup<String>("language", PropertyModel.<String>of(presentation, "language"));
            group.add(new Radio<String>("pl", Model.<String>of("PL")), new Radio<String>("en", Model.<String>of("EN")));
            return group;
        }

        private WebMarkupContainer moviesLinks(Presentation presentation) {
            WebMarkupContainer movies = new WebMarkupContainer("movies");
            movies.setVisible(forAdmin);
            movies.add(textField("youTube", propertyModel(presentation, "youTube")));
            movies.add(textField("parleys", propertyModel(presentation, "parleys")));
            return movies;
        }

        @Override
        protected void onSubmit() {
            service.save(presentation, fileUpload.getFileUpload());
            setResponsePage(returnPage);
        }


        private class SpeakersList extends PropertyListView<Speaker> {
            public SpeakersList(String id) {
                super(id, presentation.getSpeakers());
            }

            @Override
            protected void populateItem(ListItem<Speaker> item) {

                item.add(new Label("firstName"));
                item.add(new Label("lastName"));
                item.add(new Link("remove") {

                    @Override
                    public void onClick() {
                        presentation.removeSpeaker(item.getModelObject());
                        PresentationPanel.this.info("Prelegent usunięty");
                    }
                }.setVisible(presentation.notOwnedBy(item.getModelObject())));
            }
        }

        private class AddSpeakerForm extends Form {
            private Model<String> email = new Model();

            public AddSpeakerForm(String id) {
                super(id);
                add(textField("mail", email).add(RfcCompliantEmailAddressValidator.getInstance()));
                add(new SubmitLink("add") {


                    @Override
                    public void onSubmit() {

                        Speaker speaker = speakerService.findByMail(email.getObject());
                        if (speaker != null && !presentation.getSpeakers().contains(speaker)) {
                            presentation.addSpeaker(speaker);
                            PresentationPanel.this.info("Dodano prelegenta");
                            //TODO: wysłać maila przy wysłaniu głównego formularza
                        } else {
                            PresentationPanel.this.warn("Nie można dodać prelegenta");
                        }

                    }
                });
            }
        }
    }


}
