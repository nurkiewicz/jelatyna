package jelatyna.components.menu;

import org.apache.wicket.Page;


public abstract class AbstractMenuLink implements MenuLink {
    final Class<? extends Page> pageClazz;

    public AbstractMenuLink(Class<? extends Page> pageClazz) {
        this.pageClazz = pageClazz;
    }

    @Override
    public boolean containsLink(String link) {
        return getAllItems()
                .stream()
                .anyMatch(item -> link.equals(item.getTitle()));
    }

    @Override
    public Class<? extends Page> getPageClass() {
        return pageClazz;
    }

    @Override
    public boolean isLink() {
        return true;
    }

}
