package jelatyna.pages.confitura.registration;

import static jelatyna.utils.Components.*;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import jelatyna.services.RegistrationService;

@SuppressWarnings("serial")
public class RegistrationWidget extends Panel {
    @SpringBean
    private RegistrationService registrationService;

    public RegistrationWidget(String id) {
        super(id);
        add(richLabel("info", registrationService.getWidgetInfo()));
    }

}
