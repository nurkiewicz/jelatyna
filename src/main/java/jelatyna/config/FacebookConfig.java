package jelatyna.config;

import jelatyna.ConfituraSession;
import jelatyna.domain.Participant;
import jelatyna.domain.Speaker;
import jelatyna.repositories.ParticipantRepository;
import jelatyna.repositories.SpeakerRepository;
import jelatyna.security.AuthenticationUserDetails;
import org.apache.wicket.Session;
import org.apache.wicket.request.cycle.RequestCycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.social.connect.*;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.connect.support.ConnectionFactoryRegistry;
import org.springframework.social.connect.web.ProviderSignInController;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.web.context.request.NativeWebRequest;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Collection;


@Configuration
public class FacebookConfig {

    @Autowired

    @Value("${facebook.app.id}")
    private String facebookId;

    @Value("${facebook.app.secret}")
    private String facebookSecret;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private ConnectionSignUp connectionSignUp;

    @Autowired
    private SpeakerRepository speakerRepository;


    @Bean(name="connectionFactoryLocator")
    public ConnectionFactoryLocator connectionFactoryLocator() {
        ConnectionFactoryRegistry registry = new ConnectionFactoryRegistry();
        registry.addConnectionFactory(new FacebookConnectionFactory(facebookId,facebookSecret));
        return registry;
    }

    @Bean
    public UsersConnectionRepository socialConnectionRepository() {
        JdbcUsersConnectionRepository jdbcUsersConnectionRepository = new JdbcUsersConnectionRepository(dataSource, connectionFactoryLocator(), Encryptors.noOpText());
        jdbcUsersConnectionRepository.setConnectionSignUp(connectionSignUp);
        return jdbcUsersConnectionRepository;
    }

    /**
     * The Spring MVC Controller that allows users to sign-in with their provider accounts.
     */
    @Bean
    public ProviderSignInController providerSignInController() {
        ProviderSignInController providerSignInController = new ProviderSignInController(connectionFactoryLocator(), socialConnectionRepository(),
                new SimpleSignInAdapter());
//        providerSignInController.setPostSignInUrl("/c4p/speaker/view");
//        providerSignInController.setSignInUrl("/c4p/speaker/register");
//        providerSignInController.setSignUpUrl("/c4p/speaker/view");
        return providerSignInController;
    }

    private class SimpleSignInAdapter implements SignInAdapter {
        public String signIn(String userId, Connection<?> connection, NativeWebRequest request) {
            boolean userRegisteredBefore = true;
            UserProfile userProfile = connection.fetchUserProfile();
            Speaker speaker = speakerRepository.findByMail(userProfile.getEmail());
            //not in database yet, create one to fill register form
            if(speaker == null){
                userRegisteredBefore = false;
                speaker = new Speaker();
                speaker.mail(userProfile.getEmail());
                speaker.firstName(userProfile.getFirstName());
                speaker.lastName(userProfile.getLastName());
                speaker.setFacebookAccount(true);
            }
            //authorize user in spring security
            AuthenticationUserDetails speakerUserDetail = new AuthenticationUserDetails(speaker);
            Authentication auth = new UsernamePasswordAuthenticationToken(speakerUserDetail, null, speakerUserDetail.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(auth);

            return userRegisteredBefore ? "/c4p/speaker/view": "/c4p/speaker/register";
        }
    }
}
