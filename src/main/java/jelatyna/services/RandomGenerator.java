package jelatyna.services;

import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Random;

@Component
public class RandomGenerator {
    private StrongPasswordEncryptor encryptor = new StrongPasswordEncryptor();

    public String getRandomId() {
        String id = System.currentTimeMillis() + "-" + new Random().nextInt(100);
        id = encryptor.encryptPassword("" + id);
        return replaceUrlUnfriendlyChars(id);
    }

    String replaceUrlUnfriendlyChars(String id) {
        return id.replaceAll("/", "").replaceAll("\\+", "");
    }

    public int getRandomIdx(List<?> objects) {
        return new Random().nextInt(objects.size());
    }

    public <T> List<T> shuffle(List<T> objects) {
        Collections.shuffle(objects);
        return objects;
    }
}
