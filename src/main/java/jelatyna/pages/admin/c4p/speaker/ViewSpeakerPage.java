package jelatyna.pages.admin.c4p.speaker;

import jelatyna.components.user.speaker.SpeakerPanel;
import jelatyna.domain.Speaker;
import jelatyna.pages.admin.AdminBasePage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.SpeakerService;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

@SuppressWarnings("serial")
public class ViewSpeakerPage extends AdminBasePage {
    @SpringBean
    private SpeakerService service;

    public ViewSpeakerPage(PageParameters params) {
        Speaker speaker = service.findById(params.get("id").toInteger());
        add(new SpeakerPanel(speaker, service, false));
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.CALL_4_PAPERS;
    }

    @Override
    public String getPageTitle() {
        return "Prelegenci";
    }

}
