package jelatyna.components.menu;

import jelatyna.domain.MenuLinkItem;
import jelatyna.repositories.MenuLinkItemRepository;
import org.apache.wicket.Page;

import java.util.List;

import static com.google.common.collect.Lists.*;


public class SingleMenuLink extends AbstractMenuLink {

    private List<MenuLinkItem> items;

    public SingleMenuLink(MenuLinkItemRepository menuLinkItemRepository, String title, Class<? extends Page> pageClazz) {
        super(pageClazz);
        MenuLinkItem menuLinkItem = menuLinkItemRepository.readByTitle(title);
        if (menuLinkItem == null) {
            menuLinkItem = new MenuLinkItem(null, title);
        }
        this.items = newArrayList(menuLinkItem);
    }

    @Override
    public List<MenuLinkItem> getAllItems() {
        return items;
    }
}
