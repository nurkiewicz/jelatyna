package jelatyna.utils;

import org.apache.wicket.markup.html.form.upload.FileUpload;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

@SuppressWarnings("serial")
public class ImageIO implements Serializable {

    public BufferedImage read(FileUpload uplad) {
        try (InputStream inputStream = uplad.getInputStream()) {
            return javax.imageio.ImageIO.read(inputStream);
        } catch (IOException ex) {
            throw new RuntimeException("Exception On reading photo", ex);
        }
    }

}
