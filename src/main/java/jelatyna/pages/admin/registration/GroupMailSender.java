package jelatyna.pages.admin.registration;

import com.google.common.collect.Iterables;
import jelatyna.domain.Participant;
import jelatyna.pages.confitura.registration.process.CancelRegistrationPage;
import jelatyna.pages.confitura.registration.process.FinallyConfirmRegistrationPage;
import jelatyna.services.AbsoluteUrlProvider;
import jelatyna.utils.MailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.lang.String.*;

@Component
public class GroupMailSender {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private MailSender sender;
    @Autowired
    private AbsoluteUrlProvider urlProvider;
    private String approveLink;
    private String cancelLink;
    private long delay = 30000;

    public GroupMailSender(int delay) {
        this.delay = delay;
    }

    public GroupMailSender() {
    }

    public void sendMessages(List<Participant> participants) {
        sender.setTemplateName("group_mail");
        approveLink = getApproveLink();
        cancelLink = getCancelLink();
        doSendMessages(participants);
    }

    @Async
    private void doSendMessages(List<Participant> participants) {
        for (List<Participant> subList : Iterables.partition(participants, 50)) {
            for (Participant participant : subList) {
                logSending(participants, participant);
                sendMessage(participant);
            }
            doWait();
        }
    }

    private void sendMessage(Participant participant) {
        sender.set("firstName", participant.getFirstName());
        sender.set("lastName", participant.getLastName());
        sender.set("confirmLink", approveLingFor(participant));
        sender.set("cancelLink", cancelLinkFor(participant));
        sender.sendMessage(participant);
    }

    private String cancelLinkFor(Participant participant) {
        return cancelLink + participant.getToken();
    }

    private String approveLingFor(Participant participant) {
        return approveLink + participant.getToken();
    }

    private String getApproveLink() {
        return urlProvider.getPathFor(FinallyConfirmRegistrationPage.class);
    }

    private String getCancelLink() {
        return urlProvider.getPathFor(CancelRegistrationPage.class);
    }

    private void logSending(List<Participant> participants, Participant participant) {
        logger.info(format("%s/%s Sending email to %s",
                participants.indexOf(participant) + 1, participants.size(), participant.getMail()));
    }

    private void doWait() {
        try {
            Thread.sleep(delay);
        } catch (Exception ex) {
        }
    }
}
