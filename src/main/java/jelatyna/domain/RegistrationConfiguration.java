package jelatyna.domain;

import javax.persistence.Entity;
import javax.persistence.Lob;

@SuppressWarnings("serial")
@Entity
public class RegistrationConfiguration extends AbstractEntity<RegistrationConfiguration> {
    private boolean active;
    @Lob
    private String closeInfo;
    @Lob
    private String widgetInfo;

    public boolean isActive() {
        return active;
    }

    public boolean isClosed() {
        return active == false;
    }

    public String getCloseInfo() {
        return closeInfo;
    }

    public String getWidgetInfo() {
        return widgetInfo;
    }

    public RegistrationConfiguration widgetInfo(String widgetInfo) {
        this.widgetInfo = widgetInfo;
        return this;
    }

    public RegistrationConfiguration active(boolean active) {
        this.active = active;
        return this;
    }
}
