package jelatyna.pages.confitura.news;

import jelatyna.WicketTest;
import jelatyna.domain.News;
import jelatyna.services.FileService;
import jelatyna.services.NewsService;
import jelatyna.services.SpeakerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static jelatyna.TestUtils.*;
import static jelatyna.utils.PageParametersBuilder.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NewsDetailsPageShould extends WicketTest {
    @Mock
    private NewsService newsService;

    private News news = new News().id(1).title("Any title").autor(admin());

    @Override
    public void setupServices() {
        put(newsService, mock(SpeakerService.class), mock(FileService.class));

        when(newsService.findPublishedById(anyInt())).thenReturn(news);
        when(newsService.findByTitle(anyString())).thenReturn(news);
    }

    @Test
    public void loadNewsByTitleWhenOnlyTitleProvided() {
        // Given
        String title = "Any Title";

        // When
        tester.startPage(new NewsDetailsPage(params().title(title).build()));

        // Then
        verify(newsService).findByTitle(title);
        verify(newsService, never()).findPublishedById(anyInt());
    }

    @Test
    public void loadNewsByIdWhenBothIdAndTitleProvided() {
        // Given
        String title = "Any Title";
        int id = 10;

        // When
        tester.startPage(new NewsDetailsPage(params().id(id).title(title).build()));

        // Then
        verify(newsService).findPublishedById(id);
        verify(newsService, never()).findByTitle(anyString());
    }
}