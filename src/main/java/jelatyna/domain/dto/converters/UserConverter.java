package jelatyna.domain.dto.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jelatyna.domain.User;
import jelatyna.domain.dto.UserDto;
import jelatyna.services.FileService;

@Component
public class UserConverter {
    @Autowired
    private FileService fileService;

    public UserDto map(User<?> user) {
        return new UserDto()
                .id(user.getId())
                .fullName(user.getFirstName(), user.getLastName())
                .photo(fileService.getUrlTo(user))
                .bio(user.getBio())
                .twitter(user.getTwitter())
                .url(user.getWebPage());
    }
}
