package jelatyna.pages.admin.registration;

import jelatyna.ConfituraSession;
import jelatyna.WicketTest;
import jelatyna.domain.Admin;
import jelatyna.repositories.RegistrationConfigurationRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationSettingsPageShould extends WicketTest {
    @Mock
    private RegistrationConfigurationRepository repository;

    @Test
    public void startPage() {
        put(repository);
        loginAsAdmin();

        tester.startPage(RegistrationSettingsPage.class);
    }

    private void loginAsAdmin() {
        ConfituraSession.get().setUser(new Admin("test user"));
    }

}
