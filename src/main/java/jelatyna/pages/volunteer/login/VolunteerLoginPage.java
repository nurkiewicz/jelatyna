package jelatyna.pages.volunteer.login;

import jelatyna.domain.Volunteer;
import jelatyna.pages.admin.login.LoginPage;
import jelatyna.pages.volunteer.VolunteerHomePage;
import jelatyna.services.VolunteerService;
import org.apache.wicket.spring.injection.annot.SpringBean;

@SuppressWarnings("serial")
public class VolunteerLoginPage extends LoginPage {

    @SpringBean
    private VolunteerService service;

    public VolunteerLoginPage() {
    }

    @Override
    protected void tryToLogin() {
        Volunteer volunteer = service.loginWith(email, password);
        getSession().setUser(volunteer);
        setResponsePage(VolunteerHomePage.class);
    }
}
