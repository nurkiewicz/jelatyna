package jelatyna.pages.confitura.c4p.voting;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jelatyna.ConfituraSession;
import jelatyna.domain.Presentation;
import jelatyna.domain.Vote;
import jelatyna.services.RandomGenerator;
import jelatyna.services.SpeakerService;

@Component
public class Voting {
    static final String ID_KEY = "id";
    @Autowired
    private SpeakerService service;
    @Autowired
    private RandomGenerator randomizeProvider;
    @Autowired
    private HttpProvider httpProvider;

    public List<Presentation> fetchPresentations() {
        List<Presentation> presentations = getSession().getPresentations();
        if (presentations == null) {
            presentations = service.allShuffledPresentations();
            getSession().setPresentation(presentations);
        }
        return presentations;
    }

    private ConfituraSession getSession() {
        return httpProvider.getSession();
    }

    public Presentation getPresentation(int idx) {
        return fetchPresentations().get(idx);
    }

    public boolean isLast(int idx) {
        return idx + 1 == fetchPresentations().size();
    }

    public boolean isActive() {
        return service.isVotingActive();
    }

    public void save(Presentation presentation, Integer rate) {
        service.save(new Vote(getUniqueId(), presentation, rate));
    }

    private String getUniqueId() {
        String id = httpProvider.getCookieValue(ID_KEY);
        if (id == null) {
            id = randomizeProvider.getRandomId();
            httpProvider.setCookieValue(ID_KEY, id);
        }
        return id;
    }
}
