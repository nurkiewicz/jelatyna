package jelatyna.domain.dto;

public class AgendaSlotDto extends AbstractDto {
    private String room;
    private AgendaPresentationDto presentation;

    public AgendaPresentationDto getPresentation() {
        return presentation;
    }

    public void setPresentation(AgendaPresentationDto presentation) {
        this.presentation = presentation;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public AgendaSlotDto room(final String room) {
        this.room = room;
        return this;
    }

    public AgendaSlotDto presentation(final AgendaPresentationDto presentation) {
        this.presentation = presentation;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AgendaSlotDto that = (AgendaSlotDto) o;

        if (presentation != null ? !presentation.equals(that.presentation) : that.presentation != null) return false;
        if (room != null ? !room.equals(that.room) : that.room != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = room != null ? room.hashCode() : 0;
        result = 31 * result + (presentation != null ? presentation.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "AgendaSlotDto{" +
                "room='" + room + '\'' +
                ", presentation=" + presentation +
                '}';
    }
}
