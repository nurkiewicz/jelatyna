package jelatyna.api;

import org.joda.time.LocalDateTime;

public class DateApi {
    private final String day;
    private final String month;
    private final String year;

    public DateApi(LocalDateTime date) {
        this.day = date.toString("dd");
        this.month = date.toString("MMM");
        this.year = date.toString("yyyy");
    }

    public String getDay() {
        return day;
    }

    public String getMonth() {
        return month;
    }

    public String getYear() {
        return year;
    }
}
