package jelatyna.services;

import jelatyna.repositories.NewsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class NewsServiceImplShould {
    @Mock
    private NewsRepository repository;
    @InjectMocks
    private NewsService newsService;

    @Test
    public void deleteNewsById() {
        Integer id = 11;

        newsService.deleteById(id);

        verify(repository).delete(id);
    }
}
