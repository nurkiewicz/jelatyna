package jelatyna.repositories;

import jelatyna.IntegrationTest;
import jelatyna.domain.News;
import org.joda.time.LocalDateTime;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static jelatyna.services.NewsService.*;
import static org.assertj.core.api.Assertions.*;

public class NewsRepositoryShould extends IntegrationTest {
    @Autowired
    private NewsRepository repository;

    @Test
    public void fetchOnlyPublishedNewsSortedByCreationDate() {
        News published1 = new News().title("n1").published(true)
                .creationDate(new LocalDateTime(2010, 10, 1, 10, 10).toDateTime().toDate());
        News published2 = new News().title("n2").published(true)
                .creationDate(new LocalDateTime(2011, 10, 1, 10, 10).toDateTime().toDate());
        repository.saveAndFlush(published1);
        repository.saveAndFlush(published2);
        repository.saveAndFlush(new News().title("n3").published(false));

        List<News> news = repository.findAll(PUBLISHED, BY_CREATION_DATE);

        assertThat(news).containsExactly(published2, published1);
    }

    @Test
    public void fetchOnlyPublishedNews() {
        News published1 = new News().title("n1").published(true)
                .creationDate(new LocalDateTime(2010, 10, 1, 10, 10).toDateTime().toDate());
        repository.saveAndFlush(published1);
        repository.saveAndFlush(new News().title("n3").published(false));

        List<News> news = repository.findAll(new PublishedNews());

        assertThat(news).containsExactly(published1);
        assertThat(repository.count(new PublishedNews())).isEqualTo(1);
    }
}
