package jelatyna.utils;

import org.junit.Test;

import java.util.Collection;

import static org.assertj.core.api.Assertions.*;

public class HtmlUtilsShould {

    @Test
    public void extractSingleImageFromSimpleHtml() {
        // Given
        String image = "http://example.com/images/image.png";

        // When
        Collection<String> list = HtmlUtils.extractImageLinksFromHtml("<img src=\"" + image + "\"/>");

        // Then
        assertThat(list).hasSize(1)
                .contains(image);
    }

    @Test
    public void findNoImagesForNullInput() {
        // When
        Collection<String> list = HtmlUtils.extractImageLinksFromHtml(null);

        // Then
        assertThat(list).isEmpty();
    }

    @Test
    public void findNoImages() {
        // When
        Collection<String> list = HtmlUtils.extractImageLinksFromHtml("<h1>Headline</h1><br/><div>Div section with text</div>");

        // Then
        assertThat(list).isEmpty();
    }

    @Test
    public void findThreeImages() {
        // Given
        String image = "http://example.com/images/image.png";
        String image2 = "http://example.com/images/image2.png";
        String image3 = "http://example.com/images/image3.png";

        // When
        Collection<String> list = HtmlUtils.extractImageLinksFromHtml("<h1>Headline</h1><br/><img src=\"" + image + "\"/>" +
                "<img src=\"" + image3 + "\"/><img src=\"" + image2 + "\"/>");

        // Then
        assertThat(list).hasSize(3).contains(image, image2, image3);
    }

    @Test
    public void ignoreDuplicateImages() {
        // Given
        String image = "http://example.com/images/image.png";
        String image2 = "http://example.com/images/image2.png";

        // When
        Collection<String> list = HtmlUtils.extractImageLinksFromHtml("<h1>Headline</h1><br/><img src=\"" + image + "\"/>" +
                "<img src=\"" + image2 + "\"/><img src=\"" + image + "\"/>");

        // Then
        assertThat(list).hasSize(2).contains(image, image2);
    }
}
