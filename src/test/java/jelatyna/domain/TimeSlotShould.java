package jelatyna.domain;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

public class TimeSlotShould {
    private AgendaItem item1 = new AgendaItem("A");
    private AgendaItem item2 = new AgendaItem("B");
    private TimeSlot timeSlot = new TimeSlot("");

    @Test
    public void haveOneItemIfAllItemsAreSame() {
        withItems(item1, item1);

        List<AgendaItem> items = timeSlot.getItems();

        assertThat(items).hasSize(1).containsExactly(item1);
    }

    @Test
    public void returnNumberOfItemsAsSpanIfAllAreSame() {
        withItems(item1, item1);

        int span = timeSlot.getSpan();

        assertThat(span).isEqualTo(2);
    }

    @Test
    public void returnAllItemsIfAllAreDifferent() {
        withItems(item1, item2);

        List<AgendaItem> item = timeSlot.getItems();

        assertThat(item).containsExactly(item1, item2);
    }

    @Test
    public void returnOneAsSpanIfAllAreDifferent() {
        withItems(item1, item2);

        int span = timeSlot.getSpan();

        assertThat(span).isEqualTo(1);
    }

    @Test
    public void returnAllItemIfOnlyOneIsDifferent() {
        withItems(item1, item1, item2);

        List<AgendaItem> items = timeSlot.getItems();

        assertThat(items).containsExactly(item1, item1, item2);
    }

    @Test
    public void returnSingleSpanIfOnlyOneIsDifferent() {
        withItems(item1, item1, item2);

        int span = timeSlot.getSpan();

        assertThat(span).isEqualTo(1);
    }

    private void withItems(AgendaItem... items) {
        for (AgendaItem item : items) {
            timeSlot.addItem(item);
        }
    }
}
