package jelatyna.domain;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.assertj.core.api.Assertions.*;

public class NewsShould {
    private GregorianCalendar calendar = new GregorianCalendar(2000, 0, 1, 20, 10);
    private News news = new News().creationDate(calendar.getTime());

    @Test
    public void formatCreationDate() {

        // when
        String formattedDate = news.formatCreatioDate();

        // then
        assertThat(formattedDate).isEqualTo("01.01.2000");
    }

    @Test
    public void formatCreationDateWithTime() {

        // when
        String formattedDate = news.formatCreatioDateWithTime();

        // then
        assertThat(formattedDate).isEqualTo("20:10 01.01.2000");
    }

    @Test
    public void haveDescriptionIfIsNotBlank() {
        news.description("description");

        boolean hasDescription = news.hasDescription();

        assertThat(hasDescription).isTrue();
    }

    @Test
    public void notHaveDescriptionIfBlank() {
        news.description("");

        boolean hasDescription = news.hasDescription();

        assertThat(hasDescription).isFalse();
    }

    @Test
    public void notHaveDescriptionIfNull() {
        news.description(null);

        boolean hasDescription = news.hasDescription();

        assertThat(hasDescription).isFalse();
    }

}
