package jelatyna.repositories;

import jelatyna.domain.Presentation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PresentationRepository extends JpaRepository<Presentation, Integer> {
    @Query("FROM Presentation WHERE accepted = true ORDER BY title")
    List<Presentation> findAllAccepted();

}
