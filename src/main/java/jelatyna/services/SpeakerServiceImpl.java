package jelatyna.services;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.domain.Vote;
import jelatyna.pages.confitura.c4p.SpeakerMailSender;
import jelatyna.pages.confitura.c4p.login.RequestPasswordResetMailSender;
import jelatyna.repositories.PresentationRepository;
import jelatyna.repositories.SpeakerRepository;
import jelatyna.repositories.UserRepository;
import jelatyna.repositories.VoteRepository;
import jelatyna.services.exceptions.UserDoesNotExistException;


@Component
public class SpeakerServiceImpl extends UserServiceImpl<Speaker> implements SpeakerService {

    static final String INVALID_MAIL = "Podany e-mail nie jest zarejestrowany";
    @Autowired
    private SpeakerRepository repository;
    @Autowired
    private PresentationRepository presentationRepository;
    @Autowired
    private SpeakerMailSender mailSender;
    @Autowired
    private RequestPasswordResetMailSender passwordMailSender;
    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private RandomGenerator randomGenerator;
    @Value("${call4papers.active}")
    private String call4PapersActive;
    @Value("${call4papers.voting}")
    private String votingActive;

    @Override
    void saveNewUser(Speaker user) {
        super.saveNewUser(user);
        mailSender.sendMessage(user);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <REPO extends UserRepository<Speaker>> REPO getRepository() {
        return (REPO) repository;
    }

    @Override
    public void delete(Presentation presentation) {
        presentationRepository.delete(presentation.getId());

        refreshSpeakerAcceptance(Optional.ofNullable(presentation.getOwner()));
    }

    private void refreshSpeakerAcceptance(Optional<Speaker> speakerData) {
        speakerData.ifPresent(speaker -> {
                    Speaker refreshedSpeaker = repository.findOne(speaker.getId());
                    if (refreshedSpeaker.isNecessaryToRefreshAcceptance()) {
                        refreshedSpeaker.updateSpeakerAcceptanceStatus();
                        repository.save(refreshedSpeaker);
                    }
                }
        );
    }

    @Override
    public List<Presentation> allPresentations() {
        return presentationRepository.findAll();
    }

    @Override
    public void toggleAcceptance(Presentation presentation) {
        presentation.toggleAccepted();
        presentationRepository.save(presentation);
        refreshSpeakerAcceptance(Optional.ofNullable(presentation.getOwner()));
    }

    @Override
    public Speaker findByToken(String token) {
        return repository.findByToken(token);
    }

    @Override
    public Speaker findByMail(String mail) {
        return repository.findByMail(mail);
    }

    @Override
    public void resetPasswordFor(Speaker speaker, String newPassword) {
        speaker.token("");
        super.resetPassword(speaker, newPassword);
    }

    @Override
    public void requestPasswordResetBy(String mail) {
        Speaker speaker = repository.findByMail(mail);
        if (speaker == null) {
            throw new UserDoesNotExistException(INVALID_MAIL);
        } else {
            speaker.token(randomGenerator.getRandomId());
            repository.save(speaker);
            passwordMailSender.sendMessage(speaker);
        }
    }

    @Override
    public boolean isCall4PapersActive() {
        return BooleanUtils.toBoolean(call4PapersActive);
    }

    @Override
    public void save(Vote vote) {
        Vote oldVote = voteRepository.findByIpAndPresentation(vote.getIp(), vote.getPresentation());
        if (oldVote != null) {
            vote.id(oldVote.getId());
        }
        voteRepository.save(vote);
    }

    @Override
    public boolean isVotingActive() {
        return BooleanUtils.toBoolean(votingActive);
    }

    @Override
    public int countVotesFor(Presentation presentation) {
        return voteRepository.findByPresentation(presentation).size();
    }

    @Override
    public double averageRateFor(Presentation presentation) {
        List<Vote> votes = voteRepository.findByPresentation(presentation);
        return calculateAverageRate(votes)
                .setScale(2, RoundingMode.HALF_DOWN).doubleValue();
    }

    private BigDecimal calculateAverageRate(List<Vote> votes) {
        return new BigDecimal(votes
                .stream()
                .mapToDouble(v -> v.getRate().doubleValue())
                .average()
                .orElse(0));
    }

    @Override
    public List<Presentation> allShuffledPresentations() {
        return randomGenerator.shuffle(allPresentations());
    }

    @Override
    public List<Speaker> allAcceptedSpeakers() {
        return repository.allAccepted();
    }

    @Override
    public Speaker randomSpeaker() {
        List<Speaker> accepted = allAcceptedSpeakers();
        if (accepted.isEmpty()) {
            return Speaker.EMPTY;
        }
        return accepted.get(randomGenerator.getRandomIdx(accepted));
    }
}
