package jelatyna.functional;

import jelatyna.WicketIntegrationTest;
import jelatyna.domain.Presentation;
import jelatyna.domain.RegistrationConfiguration;
import jelatyna.domain.Speaker;
import jelatyna.pages.admin.registration.ParticipantMailSender;
import jelatyna.pages.confitura.registration.Registration;
import jelatyna.pages.confitura.registration.RegistrationInfoPage;
import jelatyna.pages.confitura.registration.form.ParticipantDetailsEditFormPanel;
import jelatyna.pages.confitura.registration.form.RegistrationPage;
import jelatyna.repositories.ParticipantRepository;
import jelatyna.repositories.PresentationRepository;
import jelatyna.repositories.RegistrationConfigurationRepository;
import jelatyna.repositories.SpeakerRepository;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

import static jelatyna.pages.confitura.registration.form.RegistrationForm.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

public class PersonalAgendaUseCase extends WicketIntegrationTest {
    @Autowired
    private PresentationRepository presentationRepository;

    @Autowired
    private RegistrationConfigurationRepository configurationRepository;

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private SpeakerRepository speakerRepository;

    private String title1 = "title1";
    private String title2 = "title2";

    @Before
    public void registrationIsOpen() {
        Registration registration = applicationContext.getBean(Registration.class);
        registration.mailSender(mock(ParticipantMailSender.class));
        configurationRepository.save(new RegistrationConfiguration().active(true));
    }

    @Test
    public void userSeesAcceptedPresentationsDuringRegistration() {
        // given
        prepareTwoPresentations(title1, title2);

        // when
        tester.startPage(RegistrationPage.class);

        // then
        tester.assertContains(title1);
        tester.assertContains(title2);
    }

    @Test
    public void userChoosesPreferredPresentations() {
        // given
        prepareTwoPresentations(title1, title2);
        tester.startPage(RegistrationPage.class);

        // when
        FormTester formTester = fillForm();
        formTester.setValue(PERSONAL_AGENDA_FIELD_ID + ":0:" + CHOSEN_FIELD_ID, true);
        formTester.submit();

        // then
        assertNoErrors();
        Set<Presentation> preferredPresentations = participantRepository.findAll().get(0).getChosenPresentations();
        assertThat(preferredPresentations).hasSize(1);
        assertThat(preferredPresentations.iterator().next().getTitle()).isEqualTo(title1);
    }

    private FormTester fillForm() {
        FormTester formTester = tester.newFormTester("form");
        formTester.setValue(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.FIRST_NAME_FIELD_ID, "Jan");
        formTester.setValue(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.LAST_NAME_FIELD_ID, "Kowalski");
        formTester.setValue(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.MAIL_FIELD_ID, "Jan@Kowalski.pl");
        formTester.setValue(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.MAIL_REPEATED_FIELD_ID, "Jan@Kowalski.pl");
        formTester.setValue(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.CITY_FIELD_ID, "Warszawa");
        formTester.select(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.SEX_FIELD_ID, 1);
        formTester.select(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.INFO_FIELD_ID, 0);
        formTester.select(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.POSITION_FIELD_ID, 0);
        formTester.select(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.EXPERIENCE_FIELD_ID, 0);
        formTester.select(DETAILS_FORM_PANEL + ":" + ParticipantDetailsEditFormPanel.SIZE_FIELD_ID, 0);
        return formTester;
    }

    private void assertNoErrors() {
        tester.assertRenderedPage(RegistrationInfoPage.class);
    }

    private void prepareTwoPresentations(String title1, String title2) {
        Speaker speaker = new Speaker();
        speakerRepository.save(speaker);
        presentationRepository.saveAndFlush(new Presentation().setTitle(title1).owner(speaker).accepted(true));
        presentationRepository.saveAndFlush(new Presentation().setTitle(title2).owner(speaker).accepted(true));
    }
}
