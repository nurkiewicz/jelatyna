package jelatyna.services;

import javax.servlet.http.HttpServletRequest;

import org.apache.wicket.protocol.http.RequestUtils;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.http.WebRequest;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.springframework.stereotype.Service;

import jelatyna.pages.EmptyPage;
import jelatyna.pages.confitura.BaseWebPage;

@Service
public class AbsoluteUrlProvider {

    public String getPathFor(Class<? extends BaseWebPage> page) {
        return getPathFor(page, new PageParameters());
    }

    public String getPathFor(Class<? extends BaseWebPage> page, PageParameters params) {
        return RequestUtils.toAbsolutePath(getRequest().getRequestURL().toString(), getRelativePathFor(page, params));
    }

    private HttpServletRequest getRequest() {
        return (HttpServletRequest) ((WebRequest) RequestCycle.get().getRequest()).getContainerRequest();
    }

    private String getRelativePathFor(Class<? extends BaseWebPage> page, PageParameters params) {
        return new EmptyPage().urlFor(page, params).toString();
    }

}
