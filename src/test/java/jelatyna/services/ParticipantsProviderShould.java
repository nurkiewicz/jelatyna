package jelatyna.services;

import jelatyna.domain.Participant;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.*;

public class ParticipantsProviderShould {

    private static final String WARSAW = "warsaw";
    private static final String POZNAN = "poznan";
    private static final String JAN = "jan";
    private static final String ROMAN = "roman";
    private static final String STEFAN = "stefan";

    private ParticipantsProvider provider;

    @Test
    public void groupByCity() {
        //given
        List<Participant> participants = Arrays.asList(participant("", WARSAW),
                participant("", POZNAN),
                participant("", POZNAN));

        //when
        Map<String, List<Participant>> groupedParticipants = participants
                .stream()
                .collect(Collectors.groupingBy(Participant::getLowerCaseCity));

        //then
        assertThat(groupedParticipants).hasSize(2);
        assertThat(groupedParticipants).containsKeys(WARSAW, POZNAN);
        assertGroup(groupedParticipants.get(WARSAW)).hasSize(1);
        assertGroup(groupedParticipants.get(POZNAN)).hasSize(2);
    }

    @Test
    public void groupEmptyListByCity() {
        // given
        List<Participant> participants = Arrays.asList();

        // when
        Map<String, List<Participant>> groupedParticipants = participants
                .stream()
                .collect(Collectors.groupingBy(Participant::getLowerCaseCity));

        // then
        assertThat(groupedParticipants).isEmpty();
    }

    @Test
    public void groupByFirstName() {
        List<Participant> participants = Arrays.asList(participant(JAN, ""), participant(ROMAN, ""), participant(STEFAN, ""), participant(ROMAN, ""));

        // when
        Map<String, List<Participant>> groupedParticipants = participants.stream()
                .collect(Collectors.groupingBy(Participant::getFirstName));

        // then
        assertThat(groupedParticipants).hasSize(3);
        assertThat(groupedParticipants).containsKeys(JAN, ROMAN, STEFAN);
        assertGroup(groupedParticipants.get(ROMAN)).hasSize(2);
        assertGroup(groupedParticipants.get(JAN)).hasSize(1);
        assertGroup(groupedParticipants.get(STEFAN)).hasSize(1);
    }

    private Participant participant(String firstName, String city) {
        return new Participant(firstName, firstName + "ski").city(city);
    }

    private GroupChecker assertGroup(List<Participant> group) {
        return new GroupChecker(group);
    }

    private class GroupChecker {

        private final List<Participant> group;

        private GroupChecker(List<Participant> group) {
            this.group = group;
        }

        public GroupChecker hasSize(int size) {
            assertThat(group.size()).isEqualTo(size);
            return this;
        }
    }
}
