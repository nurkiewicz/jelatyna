package jelatyna.domain;

import javax.persistence.MappedSuperclass;

import org.springframework.data.jpa.domain.AbstractPersistable;

@SuppressWarnings("serial")
@MappedSuperclass
public abstract class AbstractEntity<ENTITY> extends AbstractPersistable<Integer> {

    @SuppressWarnings("unchecked")
    public ENTITY id(Integer id) {
        setId(id);
        return (ENTITY) this;
    }

    public boolean isNotNew() {
        return isNew() == false;
    }


}
