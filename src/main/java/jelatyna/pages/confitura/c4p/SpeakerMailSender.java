package jelatyna.pages.confitura.c4p;

import jelatyna.domain.Speaker;
import jelatyna.utils.MailSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SpeakerMailSender {
    @Autowired
    private MailSender sender;

    // @Autowired
    // private AdminSpeakerMailSender adminSender;

    public SpeakerMailSender() {
    }

    public void sendMessage(Speaker speaker) {
        setupTemplate(speaker);
        sender.sendMessage(speaker);
        // adminSender.sendMessage(speaker);
    }

    private void setupTemplate(Speaker speaker) {
        sender.setTemplateName("speaker");
        sender.set("firstName", speaker.getFirstName());
        sender.set("lastName", speaker.getLastName());
    }

}
