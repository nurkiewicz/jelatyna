package jelatyna.api;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Maps;

import jelatyna.domain.Participant;
import jelatyna.domain.RegistrationStatus;
import jelatyna.domain.dto.converters.PresentationConverter;
import jelatyna.pages.confitura.registration.Registration;

@Controller
public class RegistrationApi {

    @Autowired
    private Registration registration;

    @Autowired
    private PresentationConverter converter;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getConfiguration() {
        HashMap<String, Object> map = Maps.newHashMap();
        map.put("presentations", converter.mapAll(registration.allPresentations()));
        map.put("isActive", registration.isActive());
        map.put("isFull", registration.isActive());
        return map;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<String> register(@RequestBody Participant participant) {
        try {
            registration.register(participant);
        } catch (Exception ex) {
            return new ResponseEntity(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/register/confirm/{token}", method = RequestMethod.POST)
    @ResponseBody
    public void register(@PathVariable String token) {
        registration.changeStatusFor(token, RegistrationStatus.CONFIRMED);
    }
}