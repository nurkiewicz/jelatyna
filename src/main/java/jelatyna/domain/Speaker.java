package jelatyna.domain;

import static com.google.common.collect.Lists.*;
import static java.lang.Math.*;
import static java.util.stream.Collectors.*;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.jsoup.Jsoup;

@SuppressWarnings("serial")
@Entity
public class Speaker extends User<Speaker> implements WithMail, WithFile {
    public static final Speaker EMPTY = new Speaker();
    static final int SHORT_BIO_LENGTH = 100;

    @LazyCollection(LazyCollectionOption.FALSE)
    @ManyToMany
    @JoinTable(name = "PRESENTATION_SPEAKER",
            joinColumns = {@JoinColumn(name = "SPEAKER_ID", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "PRESENTATION_ID", nullable = false, updatable = false)})
    private final List<Presentation> presentations = newArrayList();

    private String token;
    private boolean accepted = false;
    @Enumerated(EnumType.STRING)
    private BioLocation bioLocation = BioLocation.LEFT;

    public Speaker() {
    }

    public Speaker(Integer id) {
        id(id);
    }

    public List<Presentation> getPresentations() {
        return presentations;
    }

    public Speaker addPresentation(Presentation presentation) {
        presentation.owner(this);
        this.presentations.add(presentation);
        return this;
    }

    public Speaker token(String token) {
        this.token = token;
        return this;
    }

    public String getToken() {
        return token;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public Speaker accepted(boolean accepted) {
        this.accepted = accepted;
        return this;
    }

    public Speaker toggleAcceptance() {
        this.accepted = !accepted;
        return this;
    }

    public boolean anyPresentationAccepted() {
        return getAcceptedPresentations().size() > 0;
    }

    public List<Presentation> getAcceptedPresentations() {
        return presentations.stream().filter(p -> p.isAccepted()).collect(toList());
    }

    public String getShortBio() {
        String cleanBio = "";
        if (StringUtils.isNotEmpty(getBio())) {
            cleanBio = Jsoup.parse(getBio()).text();
        }
        return cleanBio.substring(0, min(SHORT_BIO_LENGTH, cleanBio.length()));
    }

    public boolean isNecessaryToRefreshAcceptance() {
        return (accepted && !anyPresentationAccepted());
    }

    public void updateSpeakerAcceptanceStatus() {
        accepted(accepted && anyPresentationAccepted());
    }

    public BioLocation getBioLocation() {
        return bioLocation;
    }

    public Speaker bioLocation(BioLocation bioLocation) {
        this.bioLocation = bioLocation;
        return this;
    }

    @Override
    public String getSubfolder() {
        return "speaker/" + getId();
    }

}
