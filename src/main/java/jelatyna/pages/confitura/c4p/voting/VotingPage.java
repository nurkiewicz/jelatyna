package jelatyna.pages.confitura.c4p.voting;

import jelatyna.components.user.UserInfoPanel;
import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.pages.PageNotFound;
import jelatyna.pages.confitura.BaseWebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.Radio;
import org.apache.wicket.markup.html.form.RadioGroup;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;
import static jelatyna.utils.PageParametersBuilder.*;

@SuppressWarnings("serial")
public class VotingPage extends BaseWebPage {
    @SpringBean
    private Voting voting;

    public VotingPage(PageParameters params) {
        int idx = params.get("id").toInt();
        Presentation presentation = voting.getPresentation(idx);
        add(new FeedbackPanel("feedback"));
        add(new VotingForm(idx, presentation));
        add(label("title", presentation.getTitle()));
        add(label("author", presentation.getOwner().getFullName()));
        add(label("language", presentation.getLanguage()));
        add(label("short-desc", presentation.getShortDescription()));
        add(richLabel("full-desc", presentation.getDescription()));
        add(new UserInfoPanel<Speaker>("speaker", presentation.getOwner()));
        if (voting.isActive() == false) {
            setResponsePage(PageNotFound.class);
        }
    }

    private final class VotingForm extends Form<Void> {
        private final int idx;
        private RadioGroup<Integer> votes = new RadioGroup<Integer>("votes", new Model<Integer>());
        private Presentation presentation;

        private VotingForm(int idx, Presentation presentation) {
            super("form");
            this.idx = idx;
            this.presentation = presentation;
            add(votes);
            createRadioButtons();
        }

        private void createRadioButtons() {
            for (int i = 1; i <= 3; i++) {
                votes.add(new Radio<Integer>(i + "", new Model<Integer>(i)));
            }
        }

        @Override
        protected void onSubmit() {
            saveIfRated();
            redirectToNewPage();
        }

        private void redirectToNewPage() {
            if (voting.isLast(idx)) {
                setResponsePage(VotingFinishedPage.class);
            } else {
                setResponsePage(VotingPage.class, paramsFor("id", idx + 1));
            }
        }

        private void saveIfRated() {
            Integer rate = votes.getModelObject();
            if (rate != null) {
                voting.save(presentation, rate);
            }
        }
    }
}
