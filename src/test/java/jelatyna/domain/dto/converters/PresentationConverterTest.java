package jelatyna.domain.dto.converters;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.domain.User;
import jelatyna.domain.dto.PresentationDto;
import jelatyna.domain.dto.UserDto;

@RunWith(MockitoJUnitRunner.class)
public class PresentationConverterTest {
    @Mock
    private UserConverter userConverter;
    @InjectMocks
    private PresentationConverter converter;

    @Test
    public void convert_presentation_to_dto() {
        UserDto user = new UserDto();
        when(userConverter.map(any(User.class))).thenReturn(user);
        Presentation presentation = new Presentation().id(1).title("title").description("rats in a cage").shortDescription("rats").addSpeaker(new Speaker());

        PresentationDto dto = converter.map(presentation);

        assertThat(dto.getId()).isEqualTo(1);
        assertThat(dto.getTitle()).isEqualTo("title");
        assertThat(dto.getLongDesc()).isEqualTo("rats in a cage");
        assertThat(dto.getShortDesc()).isEqualTo("rats");
        assertThat(dto.getSpeakers()).isSameAs(user);

    }
    @Test
    public void convert_presentation_with_two_speakers() {
        Speaker speaker1 = new Speaker(1);
        Speaker speaker2 = new Speaker(2);
        UserDto user1 = new UserDto().id(1);
        UserDto user2 = new UserDto().id(2);
        when(userConverter.map(speaker1)).thenReturn(user1);
        when(userConverter.map(speaker2)).thenReturn(user2);
        Presentation presentation = new Presentation().id(100).addSpeaker(speaker1).addSpeaker(speaker2);

        PresentationDto dto = converter.map(presentation);

        assertThat(dto.getSpeakers()).containsExactly(user1, user2);

    }
}