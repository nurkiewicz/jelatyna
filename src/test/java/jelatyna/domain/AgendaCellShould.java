package jelatyna.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class AgendaCellShould {
    @Test
    public void havePresentationIfPresentationIdSet() {
        AgendaCell Cell = new AgendaCell(0, 0, 1);

        boolean hasPresentation = Cell.hasPresentation();

        assertThat(hasPresentation).isTrue();
    }

    @Test
    public void notHavePresentationIfValueSet() {
        AgendaCell Cell = new AgendaCell(0, 0, "value");

        boolean hasPresentation = Cell.hasPresentation();

        assertThat(hasPresentation).isFalse();
    }
}
