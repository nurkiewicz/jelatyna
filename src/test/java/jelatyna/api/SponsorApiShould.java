package jelatyna.api;

import static com.google.common.collect.Lists.*;
import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;

import jelatyna.domain.Sponsor;
import jelatyna.domain.SponsorType;
import jelatyna.domain.dto.SponsorDto;
import jelatyna.services.FileService;
import jelatyna.services.SponsorService;

@RunWith(MockitoJUnitRunner.class)
public class SponsorApiShould {
    @Mock
    private SponsorService service;

    @Mock
    private FileService fileService;

    @InjectMocks
    private SponsorApi api;

    @Test
    public void convert_one_sponsor_in_type() {
        withTypes("gold");
        withSponsors(sponsor("sponsor", "web-page", "gold", "logo"));

        Map<String, List<SponsorDto>> dtos = api.allSponsors();

        assertThat(dtos.keySet()).containsOnly("gold");
        assertThat(dtos.get("gold")).containsOnly(new SponsorDto("sponsor", "web-page", "", "logo"));
    }

    @Test
    public void convert_two_sponsors_in_type() {
        withTypes("gold");
        withSponsors(sponsor("sponsor", "web-page", "gold", "logo"), sponsor("sponsor2", "web-page2", "gold", "logo2"));

        Map<String, List<SponsorDto>> dtos = api.allSponsors();

        assertThat(dtos.get("gold"))
                .containsOnly(new SponsorDto("sponsor", "web-page", "", "logo"), new SponsorDto("sponsor2", "web-page2", "", "logo2"));
    }

    @Test
    public void sort_sponsors_in_type_by_money_desc() {
        withTypes("gold");
        withSponsors(
                sponsor("1", "gold", 1L),
                sponsor("3", "gold", 3L),
                sponsor("2", "gold", 2L)
        );

        Map<String, List<SponsorDto>> dtos = api.allSponsors();

        assertThat(dtos.get("gold")).extracting("name").containsExactly("3", "2", "1");
    }

    @Test
    public void convert_sponsors_in_two_types() {
        withTypes("gold", "silver");
        withSponsors(sponsor("sponsor", "web-page", "gold", "logo"), sponsor("sponsor2", "web-page2", "silver", "logo2"));

        Map<String, List<SponsorDto>> dtos = api.allSponsors();

        assertThat(dtos.keySet()).containsExactly("gold", "silver");
        assertThat(dtos.get("gold")).containsOnly(new SponsorDto("sponsor", "web-page", "", "logo"));
        assertThat(dtos.get("silver")).containsOnly(new SponsorDto("sponsor2", "web-page2", "", "logo2"));
    }

    private OngoingStubbing<List<SponsorType>> withTypes(String... types) {
        return when(service.findDisplayableSponsorTypes())
                .thenReturn(newArrayList(types).stream().map(SponsorType::new).collect(Collectors.toList()));
    }

    private Sponsor sponsor(String name, String webPage, String type, String logoUrl) {
        Sponsor sponsor = new Sponsor(name, webPage).sponsorType(new SponsorType(type)).money(1L).description("");
        when(fileService.getUrlTo(sponsor)).thenReturn(logoUrl);
        return sponsor;
    }

    private Sponsor sponsor(String name, String type, Long money) {
        return sponsor(name, "", type, "").money(money);
    }

    private OngoingStubbing<List<Sponsor>> withSponsors(Sponsor... sponsors) {
        return when(service.findAllSponsors()).thenReturn(newArrayList(sponsors));
    }

}