package jelatyna.services;

import jelatyna.domain.Participant;
import jelatyna.repositories.ParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ConnectionSignUp implements org.springframework.social.connect.ConnectionSignUp{
    @Autowired
    private ParticipantRepository participantRepository;
    @Override
    public String execute(Connection<?> connection) {
        UUID generatedId = UUID.randomUUID();
        return generatedId.toString();

    }
}
