package jelatyna.pages.confitura;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.util.file.File;
import org.apache.wicket.util.file.Folder;

@SuppressWarnings("serial")
public class TestPage extends WebPage {
    public TestPage() {
        add(new TestForm("form"));
    }

    private final class TestForm extends Form<Void> {
        private FileUploadField fileField = new FileUploadField("file");

        private TestForm(String id) {
            super(id);
            add(fileField);
        }

        @Override
        protected void onSubmit() {
            try {
                FileUpload fileUpload = fileField.getFileUpload();
                String path = "speakers/speaker";
                Folder folder = new Folder("", path);
                folder.mkdirs();
                File file = new File(folder, "photo.jpg");
                file.createNewFile();
                fileUpload.writeTo(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
