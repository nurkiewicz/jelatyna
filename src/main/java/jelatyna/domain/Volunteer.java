package jelatyna.domain;

import javax.persistence.Entity;

@SuppressWarnings("serial")
@Entity
public class Volunteer extends User<Volunteer> {
    public Volunteer(String mail) {
        mail(mail);
    }

    public Volunteer() {

    }

    @Override
    public String getSubfolder() {
        return "volunteer";
    }

    @Override
    public String getFileName() {
        return getId() + ".jpg";
    }

}
