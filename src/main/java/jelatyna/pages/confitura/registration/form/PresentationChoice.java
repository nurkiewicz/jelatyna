package jelatyna.pages.confitura.registration.form;

import jelatyna.domain.Presentation;

import java.io.Serializable;

@SuppressWarnings("serial")
class PresentationChoice implements Serializable {
    private Presentation presentation;
    private boolean chosen = false;

    public PresentationChoice(Presentation presentation) {
        this.presentation = presentation;
    }

    public Presentation getPresentation() {
        return presentation;
    }

    public void setPresentation(Presentation presentation) {
        this.presentation = presentation;
    }

    public boolean isChosen() {
        return chosen;
    }

    public void setChosen(boolean chosen) {
        this.chosen = chosen;
    }

    public String getSpeakerName() {
        return presentation.getOwner().getFullName();
    }

    public String getTitle() {
        return presentation.getTitle();
    }
}
