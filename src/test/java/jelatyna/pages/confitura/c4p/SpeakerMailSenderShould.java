package jelatyna.pages.confitura.c4p;

import jelatyna.domain.Speaker;
import jelatyna.utils.MailSender;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class SpeakerMailSenderShould {
    private Speaker speaker = new Speaker().firstName("Jan").lastName("Kowalski").mail("jan@kowalski.pl");
    @Mock
    private MailSender sender;
    @InjectMocks
    private SpeakerMailSender speakerMailSender = new SpeakerMailSender();

    @Test
    public void sendEmailToSpeaker() {

        speakerMailSender.sendMessage(speaker);

        verify(sender).set("firstName", speaker.getFirstName());
        verify(sender).set("lastName", speaker.getLastName());
        verify(sender).sendMessage(speaker);
    }
}
