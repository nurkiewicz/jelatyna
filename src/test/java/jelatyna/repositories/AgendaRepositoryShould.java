package jelatyna.repositories;

import jelatyna.IntegrationTest;
import jelatyna.domain.Agenda;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.springframework.util.Assert.notNull;

public class AgendaRepositoryShould extends IntegrationTest {
    @Autowired
    private AgendaRepository agendaRepository;

    @Test
    public void getNewSampleAgendaWhenRepositoryIsEmpty() {
        notNull(agendaRepository.getAgenda());
    }

    @Test
    public void saveAndGetAgenda() {
        //given
        Agenda agenda = new Agenda();

        //when
        agendaRepository.saveAgenda(agenda);
        Agenda foundAgenda = agendaRepository.getAgenda();

        //then
        assertEquals(agenda, foundAgenda);
    }
}
