package jelatyna.pages.admin.user;

import jelatyna.domain.Admin;
import jelatyna.pages.admin.AbstractAddPage;
import jelatyna.pages.admin.headerpanel.AdminMenuItemEnum;
import jelatyna.services.AdminService;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.components.user.UserFormPanel.*;

@SuppressWarnings("serial")
public class AddAdminPage extends AbstractAddPage<Admin> {

    @SpringBean
    private AdminService service;

    public AddAdminPage(PageParameters params) {
        super(params);
    }

    @Override
    protected void initPage(Admin admin) {
        add(newUserFormPanel(service, admin, ListAdminPage.class));
    }

    @Override
    protected Admin createNew() {
        return new Admin();
    }

    @Override
    protected Admin findById(int id) {
        return service.findById(id);
    }

    @Override
    public AdminMenuItemEnum getActiveMenu() {
        return AdminMenuItemEnum.ORGANIZERS;
    }

    @Override
    public String getPageTitle() {
        return "Dodaj/edytuj członka Kapituły";
    }
}
