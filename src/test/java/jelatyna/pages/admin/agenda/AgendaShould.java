package jelatyna.pages.admin.agenda;

import jelatyna.domain.Agenda;
import jelatyna.domain.AgendaCell;
import jelatyna.domain.TimeSlot;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

public class AgendaShould {
    private Agenda agenda = new Agenda();
    private AgendaCell topLeftCell = agenda.getMatrix()[0][0];
    private int rowIdx = 0;

    @Test
    public void addCellsWhenAddingTimeslots() {

        // when
        agenda.setNumberOfTimeSlots(10);

        // then
        assertThat(agenda.getMatrix()).hasSize(10);
        assertThat(agenda.getMatrix()[0][0]).isEqualTo(topLeftCell);
        verifyAllCellsAreNotNull(agenda);
    }

    @Test
    public void removeCellsWhenRemovingTimeslots() {

        // when
        agenda.setNumberOfTimeSlots(1);

        // then
        assertThat(agenda.getMatrix()).hasSize(1);
        verifyAllCellsAreNotNull(agenda);
    }

    @Test
    public void addCellsWhenAddingRooms() {

        // when
        agenda.setNumberOfRooms(10);

        // then
        for (AgendaCell[] row : agenda.getMatrix()) {
            assertThat(row).hasSize(10);
        }

        verifyAllCellsAreNotNull(agenda);
    }

    @Test
    public void removeCellsWhenRemovingRooms() {

        // when
        agenda.setNumberOfRooms(1);

        // then
        for (AgendaCell[] row : agenda.getMatrix()) {
            assertThat(row).hasSize(1);
        }

        verifyAllCellsAreNotNull(agenda);
    }

    @Test
    public void notAllowForDirectMatrixModification() {
        // given

        // when
        agenda.getMatrix()[0][0] = null;

        // then
        assertThat(agenda.getMatrix()[0][0]).isNotNull();
    }

    @Test
    public void getFirstRowAsRooms() {
        agenda.setNumberOfRooms(2);
        agenda.setCell(0, 0, "room 1");
        agenda.setCell(1, 0, "room 2");

        List<String> rooms = agenda.getRooms();

        assertThat(rooms).containsExactly("room 1", "room 2");
    }

    @Test
    public void getOneSlotWithPresentations() {
        rows(2);
        column(3);
        setRow("godzina", "room 1", "room 2");
        setRow("10:00-11:00", "title 1", "title 2");

        List<TimeSlot> slots = agenda.getSlots();

        assertThat(slots).hasSize(1);
        assertSlotContains(slots.get(0), "10:00-11:00", "title 1", "title 2");
    }

    @Test
    public void getTwoSlotsWithPresentations() {
        rows(3);
        column(3);
        setRow("godzina", "room 1", "room 2");
        setRow("10:00-11:00", "title 1", "title 2");
        setRow("11:00-12:00", "title 3", "title 4");

        List<TimeSlot> slots = agenda.getSlots();

        assertThat(slots).hasSize(2);
        assertSlotContains(slots.get(0), "10:00-11:00", "title 1", "title 2");
        assertSlotContains(slots.get(1), "11:00-12:00", "title 3", "title 4");
    }

    private void assertSlotContains(TimeSlot slot2, String time, String... contents) {
        assertThat(slot2.getTime()).isEqualTo(time);
        assertThat(slot2.getItems()).extracting("value").containsExactly((Object[]) contents);
    }

    private void rows(int rows) {
        agenda.setNumberOfTimeSlots(rows);
    }

    private void column(int columns) {
        agenda.setNumberOfRooms(columns);
    }

    private void setRow(String... cellContent) {
        for (int columnIdx = 0; columnIdx < cellContent.length; columnIdx++) {
            agenda.setCell(columnIdx, rowIdx, cellContent[columnIdx]);
        }
        rowIdx++;
    }

    private void verifyAllCellsAreNotNull(Agenda agenda) {
        assertThat(agenda.getMatrix()[0][0]).isEqualTo(topLeftCell);
        for (AgendaCell[] row : agenda.getMatrix()) {
            assertThat(row).doesNotContainNull();
        }
    }
}
