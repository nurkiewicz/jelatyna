package jelatyna.pages.confitura.c4p.presentation;

import jelatyna.domain.Presentation;
import jelatyna.domain.Speaker;
import jelatyna.pages.PageNotFound;
import jelatyna.pages.confitura.c4p.SpeakerBasePage;
import jelatyna.services.PresentationService;
import jelatyna.services.SpeakerService;
import org.apache.wicket.model.ResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;

import static jelatyna.utils.Components.*;

@SuppressWarnings("serial")
public class AddPresentationPage extends SpeakerBasePage {

    @SpringBean
    private PresentationService presentationService;
    @SpringBean
    private SpeakerService service;

    public AddPresentationPage() {
        Speaker speaker = getSession().getSpeaker();
        Presentation presentation = new Presentation();
        speaker.addPresentation(presentation);
        init(presentation);
        setPageTitlePostfix(new ResourceModel("presentation.add.label"));
        setTitle("presentation.add.label");
        if (service.isCall4PapersActive() == false) {
            setResponsePage(PageNotFound.class);
        }
    }

    public AddPresentationPage(PageParameters params) {
        Presentation presentation = presentationService.findBy(params.get("id").toInteger());
        init(presentation);
        if (loggedUserIsNotAnOwnerOf(presentation)) {
            setResponsePage(PageNotFound.class);
        }
        setTitle("presentation.edit.label");
    }

    private void setTitle(String title) {
        setPageTitlePostfix(new ResourceModel(title));
        add(i18nLabel("page.title", title));
    }

    private boolean loggedUserIsNotAnOwnerOf(Presentation presentation) {
        return presentation.getOwner().getId().equals(getSession().getSpeaker().getId()) == false;
    }

    private void init(final Presentation presentation) {
        add(PresentationPanel.forPresenter("presentation", presentation));
    }

}
