package jelatyna.repositories;

import jelatyna.domain.MenuLinkItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuLinkItemRepository extends JpaRepository<MenuLinkItem, Integer> {

    MenuLinkItem readByTitle(String title);

    MenuLinkItem readByItemId(Integer itemId);
}
