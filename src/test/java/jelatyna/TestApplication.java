package jelatyna;

import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.springframework.context.ApplicationContext;

import static org.springframework.util.Assert.*;

public class TestApplication extends Confitura {
    private ApplicationContext applicationContext;

    public TestApplication(ApplicationContext applicationContext) {
        notNull(applicationContext);
        this.applicationContext = applicationContext;
    }

    @Override
    protected void productionInit() {
        getComponentInstantiationListeners().add(new SpringComponentInjector(this, applicationContext));
    }
}