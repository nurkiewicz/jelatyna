package jelatyna.domain;

import com.google.common.base.Objects;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@SuppressWarnings("serial")
@Entity
public class Draw extends AbstractEntity<Draw> {
    @OneToOne
    private Participant participant;

    public Draw() {
    }

    public Draw(Participant participant) {
        this.participant = participant;
    }

    public Participant getParticipant() {
        return participant;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass().getName().equals(obj.getClass().getName())) {
            Draw that = (Draw) obj;
            return Objects.equal(participant, that.participant)
                    && Objects.equal(getId(), that.getId());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId(), participant);
    }

}
