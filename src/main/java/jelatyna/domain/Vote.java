package jelatyna.domain;

import com.google.common.base.Objects;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@SuppressWarnings("serial")
@Entity
public class Vote extends AbstractEntity<Vote> {

    private String ip;
    private Integer rate;
    private Integer positionInOrder;
    @ManyToOne
    private Presentation presentation;

    public Vote() {
    }

    public Vote(String ip, Presentation presentation, Integer rate) {
        this.ip = ip;
        this.presentation = presentation;
        this.rate = rate;
    }

    public Vote(String ip, Presentation presentation, Integer rate, Integer positionInOrder) {
        this(ip, presentation, rate);
        this.positionInOrder = positionInOrder;
    }

    public String getIp() {
        return ip;
    }

    public Presentation getPresentation() {
        return presentation;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public Integer getPositionInOrder() {
        return positionInOrder;
    }

    @Override
    public String toString() {
        return "Vote [ip=" + ip + ", rate=" + rate + ", presentation=" + presentation + ", order=" + positionInOrder + "]";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass().getName().equals(obj.getClass().getName())) {
            Vote that = (Vote) obj;
            return Objects.equal(getId(), that.getId())
                    && Objects.equal(getIp(), that.getIp())
                    && Objects.equal(getPresentation(), that.getPresentation())
                    && Objects.equal(getRate(), that.getRate())
                    && Objects.equal(getPositionInOrder(), that.getPositionInOrder());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId(), rate, ip, presentation, positionInOrder);
    }

}
