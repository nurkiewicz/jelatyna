package jelatyna.pages.admin.graphs;

import java.util.List;

public class Graph {

    private final List<String> xAxis;
    private final List<Integer> yAxis;

    public Graph(List<String> xAxis, List<Integer> yAxis) {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
    }

    public List<String> getXAxis() {
        return xAxis;
    }

    public List<Integer> getYAxis() {
        return yAxis;
    }

}
